<?php

defined('BASEPATH') OR exit('No direct script access allowed');



// use \net\authorize\api\contract\v1 as AnetAPI;

class App extends CI_Controller {

    private $response = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        header('Access-Control-Allow-Origin: *');
        //echo "<pre>";print_r($_SERVER);die;
        //die($this->agent->referrer());
        //if(in_array(referrer))
    }

    public function index($test = null) {

        
        
    $data = null;
    $plan = $this->input->get('p')?$this->input->get('p'):2;
	$data['token'] = $this->input->get('t')?$this->input->get('t'):'';
    $data['d'] = $this->input->get('d')?$this->input->get('d'):'';

	$this->session->set_userdata('plan',$plan);
	
    $plans = $this->config->item('PLANS');
    $data['alias'] = @$plans[$plan]['alias'];
    

    $data['paymentDone'] = $this->session->userdata('paymentDone')? :false;
    	//die('here');
        $this->load->view('index',$data);
    }

    public function pricing() {
        $this->load->view('pricing');
    }
    
    public function payment(){
    	
        if ($this->input->server('REQUEST_METHOD') != 'POST'){
            echo json_encode(['alert' => 'danger', 'flash' => 'No direct script access allowed']);
            exit();
        }

        //Test cards not allowed in live environment
        if($this->config->item('APP_ENV') == 'live'){
            $test_cards = ['4242424242424242','4111111111111111','370000000000002','6011000000000012','5424000000000015','4007000000027','4012888818888','4222222222222','3088000000000017','38000000000006','2223000010309703','2223000010309711'];

            if(in_array($this->input->post('card'), $test_cards)){
                echo json_encode(['alert' => 'danger', 'flash' => 'Enter a valid credit card number.']);
                exit;
            }
        }
        $currentDate = date('Y-m-d H:i:s');
        $userData = $this->input->post();
        //echo "<pre>";print_r($userData);die;

        $userData['dob'] = date('Y-m-d',strtotime($this->input->post('dob')));

        $plan = $this->session->userdata('plan');
        if(!$plan){
        	echo json_encode(['alert' => 'danger', 'flash' => 'No Product Selected']);
        }
        
        try{
        $this->load->library('Authorizenet', 'authorizenet');
        }catch(Exception $e){
            echo json_encode(['alert' => 'danger', 'flash' => $e->getMessage()]);
        }

        $cardExpiry = $this->input->post('expiry');
        list($expiryMonth, $expiryYear) = explode('/',$this->input->post('expiry'));

//$plan = 1;
        $product = new StdClass();
        $planData = $this->config->item('PLANS');
        $product->name = $planData[$plan]['name'];
        $product->price = $planData[$plan]['price'];
	$product->alias = $planData[$plan]['alias'];
	//echo "<pre>";print_r($userData);die;
        try{
        $subscription = $this->authorizenet->initialize('Dovly')->creditCard(
            str_replace('-','',$this->input->post('card')),
            $expiryMonth,
            '20'.$expiryYear,
            $this->input->post('cvv')
        )->recurring(
            $product,
            $userData
        )->subscriptionResponse(false);
        

        if($subscription['status'] == 'success'):
            $mcdData = [
                'user' => [
                    'uuid'=>$this->input->post('uuid')
                ],
                'order' => null,
                'product_alias' => $product->alias
            ];
            $mcdData['order'] = $subscription['data'];
            $mcdData['order']['amount_paid'] = $product->price;
            $mcdData['user']['profile_id'] = $subscription['data']['profile_id'];
            $mcdData['user']['payment_profile_id'] = $subscription['data']['payment_profile_id'];

            $mcdResponse = $this->sendRequest($mcdData);
            if($mcdResponse['result'] != 'success'){
            	$this->session->set_userdata('paymentDone',0);
            	echo json_encode(['alert' => 'danger', 'flash' => 'Payment done but could not create account.Contact administrator','data'=>$mcdResponse]);
            	}
            else{
            	$this->session->set_userdata(
            	'paymentDone',1);
            	echo json_encode(['alert' => 'success', 'flash' => 'Your account has been created, please answer few questions to get started','data'=>$mcdResponse]);
            	}
        else:
            $this->session->set_userdata('paymentDone',0);
            echo json_encode(['alert' => 'danger', 'flash' => $subscription['message']]);
            
        endif;
        } catch(Exception $e){
            $this->session->set_userdata('paymentDone',0);
            echo json_encode(['alert' => 'danger', 'flash' => $e->getMessage()]);
        }
        
    }

    private function sendRequest($data, $url = 'user/create', $debug = false){
        
        $funnelUrl = $this->config->item('API_URL');
        $funnelUrl .= "/funnel-api/".$url;
	//$funnelUrl = "http://dev.mycreditdash.com/funnel-api/user/create";
	
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $funnelUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POST, 1);
        
        $result = curl_exec($ch);

        if($debug == true)
            echo curl_error($ch);
	if(curl_error($ch)){
	   return ['result'=>'failed','data'=>curl_error($ch)];
	}
        curl_close($ch);

        return ['result'=>'success','data'=>$result];
    }
    public function step($step = 1, $action = null) {
    	$this->load->view("step$step", [
            'step' => $step,
            'action' => $action
        ]);
    }

    public function api() {

        $post = $this->input->post();
        if (empty($post))
            show_404();

        $config = array('secretKey' => $this->config->item('STRIPE_SECRET_KEY'), 'token' => $post['tokenKey']);

        $this->load->library('payment', $config);
        $productId = (int) $post['productId'] - 1;
        $subscription = $this->config->item('subscriptions');
        $price = (float) $subscription['plans']['amount'][$productId];
        $data = array(
            'price' => $price * 100,
            'plan' => "Charge for - {$subscription['plans']['label'][$productId]}"
        );
        $response = $this->payment->charge($data);
        if ($response['status'] == true):
            $post['product_id'] = $post['productId'];
            $post['charge_amount'] = $price;
            $post['payment_id'] = $response['data']['id'];
            $response = $this->apiRequest($post);
        endif;
        echo json_encode($response);
    }

    private function apiRequest($data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->config->item('apiUrl') . 'saveinfo/5');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        return json_decode($server_output);
    }

    public function userSession() {
  
        header('Access-Control-Allow-Origin: *');
        $post = $this->input->post();

        if (empty($post) || !isset($post['type']) || empty($post['type']))
            show_404();
        $this->response['alert'] = 'danger';
        $this->response['flash'] = 'Something went wrong.';
        $this->response['status_code'] = 404;
        switch ($post['type']):
            case 'set':
            	if(isset($post['customerId']))
                    $this->session->set_userdata('customerId', $post['customerId']);
                
        		switch($post['step']){
        		   case 1:
        		   	$step = 'one';
                    $s = 2;//next step
        		   break;
        		   
        		   case 2:
        		   	$step = 'two';
                    $s = 3;//next step
        		   break;
        		   
        		   case 3:
        		   	$step = 'three';
                    $s = 4;//next step
        		   break;
        		}
		
                /*if(isset($post['step']))
                    $this->session->set_userdata("$post['step']", $post);
                    */
                    
                 unset($post['step']);
                $this->session->set_userdata($step,$post);
                $this->session->set_userdata('step',$s);
                if(isset($post['token']))
                    $this->session->set_userdata('token',$post['token']);

                $this->response['alert'] = 'success';
                $this->response['flash'] = 'done';
                $this->response['status_code'] = 199;
                if($step=='three')
                	$this->response['paymentDone'] = '1';
                break;
            case 'retrieve':
                if ($this->session->has_userdata('customerId')):
                    $this->response['alert'] = 'success';
                    $this->response['flash'] = 'done';
                    $this->response['status_code'] = 200;
                    //$this->response['customerId'] = $this->session->userdata('customerId');
                    $this->response['userdata'] = $this->session->all_userdata();
                else:
                    $this->response['logout'] = $this->session->has_userdata('logout') ? $this->session->userdata('logout'): false;
                    $this->session->unset_userdata('logout');
                endif;
                break;
            case 'logout':
                $customerId = $this->session->has_userdata('customerId') ? $this->session->userdata('customerId') : null;
                if ($post['customerId'] == $customerId):
                    $this->session->sess_destroy();
                    /*foreach( $this->session->all_userdata() as $k => $v){
                        $this->session->unset_userdata($k);
                    }
                    $this->session->set_userdata('logout',true);
                    $this->response['userdata'] = $this->session->all_userdata();*/

                    $this->response['status_code'] = 201;
                    $this->response['alert'] = 'success';
                    $this->response['flash'] = 'done';
                endif;
                break;
        endswitch;
        echo json_encode($this->response);
    }

    public function isAuthorized() {
        $authToken = $this->session->has_userdata('authToken');
        if (!$authToken)
            $this->load->view('auth');
    }

}
