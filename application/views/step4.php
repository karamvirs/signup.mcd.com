<div ng-controller="ctrlStep4" id="step4-div">
    <div  ng-if="scrollup" class="scrollup alert alert-info response-alert alert-dismissible" data-auto-dismiss>
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><i class="fa fa-info-circle"></i></strong> <message>Please scroll up</message>
    </div>
<div class="page_title about-title"  ng-hide="loading">
    <div class="container">
        <h3>Step 4</h3>
        <p>{{pageTitle}}</p>
    </div>
</div>
        
<section class="contact-page" ng-hide="loading">	
    <div class="container">					  
        <div class="row">	
            <div ng-hide="termLoading" class="col-md-9 col-sm-9 step-4 terms-conditions">
                <!--<button ng-click="previousStep(3)">Back</button>-->
                <form name="termForm" id="termForm" ng-submit="termForm.$valid && termConditions()">
                    <!-- payment details -->
                    <div ng-show="!$parent.paymentDone" id="payment-fields">
                    	<div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.card.$error.required]" >
                            <label for="card">Credit Card Number:</label>
                            <input placeholder="credit card number" type="text" class="form-control" value="" input-mask="true" name="card" ng-model="formData.card" id="card_number" ng-required="!paymentDone">
                            <span ng-show="form.$submitted && form.card.$error.required" class="text text-danger">The card number field is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.expiry.$error.required]" >
                            <label for="expiry">Expiration Date:</label>
                            <input placeholder="mm/yy" type="text" class="form-control" value="" input-mask="true" name="expiry" ng-model="formData.expiry" id="expiry" ng-required="!paymentDone">
                            <span ng-show="form.$submitted && form.expiry.$error.required" class="text text-danger">The card expiry field is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.cvv.$error.required]" >
                            <label for="cvv">Security Code:</label>
                            <input type="text" class="form-control" value=""  name="cvv" ng-model="formData.cvv" id="cvv" ng-required="!paymentDone">
                            <span ng-show="form.$submitted && form.cvv.$error.required" class="text text-danger">The CVV field is required.</span>
                        </div>
                     </div>

                     <div ng-show="$parent.paymentDone">
                        <div class="form-group" style="font-weight: bold;color: green;">Your payment has been processed succesfully.
                        </div>
                    </div>
                    <!-- End payment details -->




                    <div class="form-group">
                        <div class="mt-checkbox-list">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input ng-model="termFormData.term0" required="" type="checkbox"> NOTICE TO USERS OF CONSUMER REPORTS:
                                <span></span>
                            </label>
                        </div>
                        <div class="terms">
                            <?php $this->load->view('terms1') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mt-checkbox-list">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" ng-model="termFormData.term1" required=""> I UNDERSTAND THAT BY CLICKING THE SUBMIT BUTTON IMMEDIATELY FOLLOWING THIS NOTICE, THAT I AM PROVIDING 'WRITTEN INSTRUCTIONS' TO <b>Rapid Credit Reports Inc.</b> UNDER THE FAIR CREDIT REPORTING ACT AUTHORIZING <b>Rapid Credit Reports Inc.</b> TO OBTAIN INFORMATION FROM MY PERSONAL CREDIT PROFILE OR OTHER INFORMATION FROM TRANSUNION, EQUIFAX AND EXPERIAN. I AUTHORIZE <b>Rapid Credit Reports Inc.</b> TO OBTAIN SUCH INFORMATION SOLELY TO CONFIRM MY IDENTITY.<br>
                                I understand that my subscription will continue and my card will be charged each month for the services offered until I cancel my subscription by <a target="_blank" href="https://www.creditberry.com/home/contact">Contacting Credit Berry.</a><br/>
I acknowledge that I have read the Terms & Conditions and Privacy Policy, and agree to their terms
                                <span></span>
                            </label>
                        </div>
                    </div>
                    
                    
                    
                    <div class="form-group">
                        <button ng-disabled="termForm.$invalid" ng-class="termForm.$invalid?'btn-info':'btn-primary'" class="btn">Accept &amp; Continue</button>
                    </div>
                </form>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-124">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                  Edit Personal Info
                </button>
            </div>
            <div class="col-md-8 col-sm-9 col-xs-12>
                <div class="row">

                    <div class="response"></div>
                    <!-- <div style="padding: 100px;" ng-if="loading" >
                    <p style="font-size: 50px;" class="text-center oow-questions" ng-clock>
                        <i class="fa fa-spinner fa-spin"></i>
                        
                    </p>
                    </div> -->
                    <div class="col-md-12" ng-show="quizList && !loading" ng-if="questions.length">
                        <form name="formverify" id="formverify" method="post" ng-submit="formverify.$valid && submitForm()" novalidate>
                            <div ng-repeat="question in questions" ng-init="count = 0" ng-cloak="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-lg-12" >
                                            <p id="item{{$index}}" class="q_series"><b>{{count = $index + 1}}.</b> {{question.FullQuestionText}}</p>
                                            <input type="text"  name="QuestionId" id="{{$index}}" value="{{question.QuestionId}}" style="display: none;" required="">
                                        </div>
                                        <div class="col-lg-12" id="options" ng-repeat="answer in question.AnswerChoice">
                                            <div class="iRadio">
                                                <input type="radio" class="option_{{$parent.$index}}" name="AnswerChoiceId_{{$parent.$index}}" value="{{answer.AnswerChoiceId}}" id="AnswerChoiceId_{{$index}}_{{$parent.$index}}" required="">
                                                <label for="AnswerChoiceId_{{$index}}_{{$parent.$index}}">{{answer.AnswerChoiceText}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="seperator col-lg-12">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" ng-disabled="formverify.$invalid" value="submit" class="btn btn-primary"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            

        </div>
    </div>
</section>
</div>
<!--<script type="text/javascript" src="<?= base_url() ?>/assets/theme/js/jquery.creditCardValidator.js"></script>-->
<script type="text/javascript">
    var scope = angular.element($("#body")).scope();
    stopLoading();
    function stopLoading(){
        //scope.loading = false;
    }

    


$(document).ready(function() {
    $("#expiry").mask("99/99", {placeholder: "mm/yy"});
    // $(".loan-amount").mask("$99,999,999", {placeholder: "___-___-____"});
    //card validation on input fields
    
});


</script>
<script type="text/javascript">
    $(function () {
        
    });
</script> 