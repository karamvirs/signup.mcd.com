<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" ng-if="report.score">
    <div class="dashboard-stat2 bordered credit-score-box">
        <div class="display">
            <div class="number">
                <small>Vantage 3.0(R) Credit Score</small>
                <h3 class="font-blue-sharp">
                    <span data-counter="counterup" data-value="{{report.score}}">{{report.score}}</span>
                </h3>
            </div>
            <div class="icon">
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" ng-if="report.extrainfo.TotalBalances">
    <div class="dashboard-stat2 bordered credit-item-box">
        <div class="display">
            <div class="number">
                <small>Total Balances</small>
                <h3 class="font-green-sharp">
                    {{report.extrainfo.TotalBalances | currency:'$':0}}
                </h3>
            </div>
            <div class="icon">
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" ng-if="report.extrainfo.TotalMonthlyPayments">
    <div class="dashboard-stat2 bordered credit-item-box">
        <div class="display">
            <div class="number">
                <small>Total Monthly Payments</small>
                <h3 class="font-red-haze">
                    <!--<span data-counter="counterup" data-value="1349">1349</span>-->
                    {{report.extrainfo.TotalMonthlyPayments | currency:'$':0}}
                </h3>
            </div>
            <div class="icon">
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" ng-if="report.extrainfo.OnTimePaymentPercentage">
    <div class="dashboard-stat2 bordered credit-item-box">
        <div class="display">
            <div class="number">
                <small>On Time Payment Percentage</small>
                <h3 class="font-purple-soft">
                    {{report.extrainfo.OnTimePaymentPercentage}}
                </h3>
            </div>
            <div class="icon">
            </div>
        </div>
    </div>
</div>