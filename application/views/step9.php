<section class="hero-section">
    <!-- CONTAINER -->
    <div class="container">
        <div class="intro-section  text-center">
            <h1 class="intro  wow fadeIn">Get Your Credit Score</h1>
            <p class="wow fadeIn">Reference site about Lorem Ipsum, giving information.</p>			
            <div class="intro-features col-md-10 col-md-offset-1">		   
                <div class=" feature">1-day loream ipsum  <strong>Feb 10</strong></div>
                <div class="feature">Refrense <strong>$00.95 </strong><br> automatically charged after free trial</div>
                <div class="feature">For questions or to cancel, just <br><strong>Call 88-888-8888</strong></div>   
            </div>
            <div class="clearfix"></div>
            <div class="form-with-features"> 
                <div class="col-md-4 col-sm-4 wow flipInX">
                    <div class="login-form side-box left">
                        <div class="head">Sample Score</div>
                        <img src="<?= base_url() . 'assets/images/Laptop.png' ?>">
                    </div>				 
                </div>				 
                <div class="col-md-4 col-sm-4">	               			 
                    <form class="form-horizontal login-form wow flipInX form-horizontal">	
                        <div class="head">Starts Here</div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mar_right_0">
                                    <i class="ion-android-person"></i>
                                    <input type="text" class="form-control" placeholder="First Name" name="First_Name" id="First_Name">
                                </div>	
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <i class="ion-android-person"></i>
                                    <input type="text" class="form-control" placeholder="Last Name" name="Last_Name" id="Last_Name">
                                </div>
                            </div>		
                        </div>
                        <div class="form-group">
                            <i class="ion-ios-email"></i>
                            <input type="text" placeholder="Email" class="form-control" name="Email" id="Email">
                        </div>
                        <div class="form-group">
                            <i class="ion-ios-telephone-outline"></i>
                            <input type="text" placeholder="Phone Number" class="form-control" name="Phone" id="Phone">
                        </div>
                        <div class="form-group">
                            <a href="" class="btn btn-orange btn-lg btn-block">Get Started - Now</a>
                        </div>
                    </form>	
                </div>	<!--- col-md-5-->	
                <div class="col-md-4 col-sm-4 wow flipInX">
                    <div class="login-form side-box right">
                        <div class="head">Benefits</div>
                        <ul class="features-list">
                            <li><strong>Loream Ipsum </strong>Dollar sit amate</li>
                            <li><strong>Loream Ipsum</strong> Dollar sit amate</li>
                            <li><strong>Loream Ipsum</strong> Dollar sit amate</li>
                            <li><strong>Loream Ipsum</strong> Dollar sit amate</li>
                            <li><strong>Loream Ipsum</strong> Dollar sit amate</li>
                        </ul>
                    </div>				 
                </div>	
            </div>  <!-- form-with-features -->
        </div><!----intro-section-->
        <!--- col-md-5-->	
    </div><!----container-->
</section>
<!--- -------------testimonials END--------------- -->
<div class="clearfix"></div>
<section class="Sign-up ">
    <div class="container text-center">		     		
        <div class="page-title  wow fadeIn">		 
            <h1>Protect Your Future with Lorem</h1>
            <p>Reference site about Lorem Ipsum, giving information</p><br>			 
            <img src="<?= base_url() . 'assets/images/customers.png' ?>" class="beauro">			 
        </div>	
        <a class="btn btn-primary btn-lg wow fadeIn" href="">Get Started Now!</a>
        <div class="clearfix"></div>	
    </div><!----container-->		
</section>





