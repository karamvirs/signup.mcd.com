<div ng-controller="ctrlStep1">
<div class="page_title about-title" ng-hide="loading">
    <div class="container">
        <h3>Step 1</h3>
        <p>We will use the information below to create an account for you and pull your credit report.</p>
    </div>
</div>

<section class="contact-page" ng-hide="loading">	
    <div class="container">					  
        <div class="row">					  			   
            <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2">
                <form class="form-horizontal common-form wow fadeIn" id="ccfunnel-form-step1" method="post" name="form" ng-submit="submitForm(form.$valid)" novalidate>
                    <div class="row"   ng-if="!loading">
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.firstname.$invalid]">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="firstname" placeholder="First Name" id="First_Name" ng-model="formData.firstname" required autocomplete="off">
                            <span ng-show="form.$submitted && form.firstname.$invalid" class="help-inline error">First Name is required.</span>
                        </div>	
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.lastname.$invalid]">
                            <label>Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name" name="lastname" ng-model="formData.lastname" required autocomplete="off">
                            <span ng-show="form.$submitted && form.lastname.$invalid" class="help-inline error">Last Name is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.email.$invalid]">
                            <label>Email Address</label>
                            <input type="email" placeholder="Email Address" class="form-control" id="email" name="email" ng-model="formData.email" required autocomplete="off">
                            <span ng-show="form.$submitted && form.email.$invalid" class="help-inline error">Valid Email is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.phone.$invalid]">
                            <label for="phone">Phone Number:</label>
                            <input type="text" class="form-control" ng-model="formData.phone" id="phone" ng-minlength="10"  name="phone" required autocomplete="off">
                            <span ng-show="form.$submitted && form.phone.$invalid" class="help-inline error">Valid phone is required.</span>
                        </div>
                        <div class="form-group">

                            <button type="submit" ng-class="form.$invalid || loading || form.email.$invalid?'btn-warning':'btn-primary'" ng-disabled="form.$invalid || loading || form.email.$invalid" class="btn btn-block btn-lg">Get Started - Now</button>
                        </div>
                        
                    </div>
                    
                </form>	
            </div>	
        </div>
    </div>
</section>
</div>

<script type="text/javascript">
    $(function () {
        // $("#phone").mask("999-999-9999", {placeholder: "___-___-____"});
        
    });

    /*var scope = angular.element($("#body")).scope()
    scope.loading = false;*/
</script> 