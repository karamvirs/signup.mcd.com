<div class="col-md-8 col-md-offset-2">
    <form method="post" name="form" ng-submit="submitForm(form.$valid)" novalidate>
        <div class="form-group">
            <label for="accessToken">Access Token:</label>
            <input type="text" class="form-control" id="accessToken" name="accessToken" ng-model="accessToken" required autocomplete="off">
            <span ng-show="form.$submitted && form.accessToken.$invalid" class="help-inline error">Valid access token is required.</span>
        </div>
        <button type="submit" class="btn btn-primary">Next</button>
    </form>
</div>