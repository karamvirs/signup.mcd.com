<!DOCTYPE html>
<html lang="en" ng-app="stepA">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MyCreditDash</title>
        <!-- Bootstrap -->
        <!--<link rel="shortcut icon" type="image/png" href="<?= base_url() ?>/assets/favicon.ico"/>-->
        <link rel="stylesheet" href="<?= base_url() ?>/assets/datepicker/bootstrap-datepicker.css">
        <link href="<?= base_url() ?>/assets/theme/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/assets/theme/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/assets/theme/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/assets/theme/css/style.css?ver=<?= rand(1234,9999) ?>" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link href="<?= base_url() ?>/assets/theme/css/responsive.css" rel="stylesheet" type="text/css" />  
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" /> 
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .ng-clock{display: none !important;}
            .ng-clock.ng-scope{display: block !important;}
        </style>
        

            <!-- Main theme files -->
            <link href="../css/style.css?ver=<?= rand(1234,9999) ?>" rel="stylesheet" type="text/css">
            <link href="../css/responsive.css?ver=<?= rand(1234,9999) ?>" rel="stylesheet" type="text/css">   
    </head>
    <body ng-controller="page_load" class="ng-clock" id="body">
        <div class="text-center step-processing loading"  ng-if="loading==true" ng-clock>
                            <i class="fa fa-spinner fa-spin" style="font-size:44px"></i>
        </div>
        <div  ng-if="page.alert && page.flash" class="alert alert-{{page.alert}} response-alert" id="message">
                <!-- <a class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
                <strong><i class="fa fa-info-circle"></i></strong> <message>{{page.flash}}</message>
            </div>
        <div class="main_wrapper transp-header">
            
            
            <ng-view></ng-view>
            
        </div>
        
        
        <!------ ===========  main_wrapper Close  ==========------>
        


        <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle" style="display: inline;">Edit Persoanl Info</h5>
        <span style="margin-left: 20px" ng-class="pi_status?alert-success:alert-danger" ng-if="pi_result">{{pi_result}}</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="text-center step-processing loading"  ng-if="pi_processing" ng-clock>
            <i class="fa fa-spinner fa-spin" style="font-size:44px"></i>
        </div>

      <div class="modal-body" ng-hide="pi_processing">
            <form class="form-horizontal common-form wow fadeIn" id="ccfunnel-form-step1" method="post" name="form" ng-submit="submitPIForm(form.$valid)" novalidate>
                    
                    <div class="row">
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.firstname.$invalid]">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="firstname" placeholder="First Name" id="First_Name" ng-model="userData.step1.firstname" required autocomplete="off">
                            <span ng-show="form.$submitted && form.firstname.$invalid" class="help-inline error">First Name is required.</span>
                        </div>  
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.lastname.$invalid]">
                            <label>Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name" name="lastname" ng-model="userData.step1.lastname" required autocomplete="off">
                            <span ng-show="form.$submitted && form.lastname.$invalid" class="help-inline error">Last Name is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.email.$invalid]">
                            <label>Email Address</label>
                            <input type="email" placeholder="Email Address" class="form-control" id="email" name="email" ng-model="userData.step1.email" required autocomplete="off">
                            <span ng-show="form.$submitted && form.email.$invalid" class="help-inline error">Valid Email is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has-error'}[form.$submitted && form.phone.$invalid]">
                            <label for="phone">Phone Number:</label>
                            <input type="text" class="form-control" ng-model="userData.step1.phone" id="phone" minlength="12"  name="phone" required autocomplete="off">
                            <span ng-show="form.$submitted && form.phone.$invalid" class="help-inline error">Valid phone is required.</span>
                        </div>
                        <div class="form-group">

                            <button type="submit" ng-class="form.$invalid || form.email.$invalid?'btn-warning':'btn-primary'" ng-disabled="form.$invalid || form.email.$invalid" class="btn btn-block btn-lg">Save</button>
                        </div>
                        
                    </div>
                    
            </form> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

        
        <script src="<?= base_url() ?>/assets/theme/js/jquery-latest.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/wow.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/jquery.maskedinput.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/custom.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
        <script src="<?= base_url() . 'assets/angular-ui.min.js' ?>"></script>
        <script src="<?= base_url() . 'assets/datepicker/bootstrap-datepicker.min.js' ?>"></script>
        <script src="<?= base_url() . 'assets/wow.min.js' ?>"></script>
        <script src="https://js.stripe.com/v3/"></script>
        <script type="text/javascript">
            var apiUrl = "<?= $this->config->item('apiUrl') ?>";
            var token = "<?= $token; ?>";
            var d = "<?= $d; ?>";
            
            
            var customerId = "<?= @$_GET['id'] ? @$_GET['id'] : '' ?>";
            var mainPage = '<?= $this->config->item('mainpage') ?>';
            var site_url = "<?= site_url() ?>/";
            customerName = '';
            var product_alias = "<?= $alias ?>";

            var paymentDone = "<?php echo $paymentDone ?>"; 
            var phone = "";
            
        </script>

        <script src="<?= base_url() . 'assets/main.js?ver='.rand(1234,9999) ?>"></script>
        <script src="<?= base_url() . 'assets/mask.js' ?>"></script>
        <!-- Start of HubSpot Embed Code --> <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4727367.js"></script> <!-- End of HubSpot Embed Code -->
    </body>
</html>