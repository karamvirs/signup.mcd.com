<div ng-controller="ctrlStep2">
<div class="page_title about-title"   ng-hide="loading">
    <div class="container">
        <h3>Step 2</h3>
        <p>Please take a moment to answer the questions below.  This will help us tailor your credit recommendations.</p>
    </div>
</div>


<section class="contact-page"  ng-hide="loading">	
    <div class="container">					  
        <div class="row">					  			   
            <!-- <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2"> -->
            <div class="col-md-9 col-sm-9 col-xs-12">
                <!--<button ng-click="previousStep(1)">Back</button>-->
                <form class="form-horizontal common-form wow fadeIn" id="ccfunnel-form-step1" method="post" name="form" ng-submit="submitForm(form.$valid)" novalidate>
                    <div class="row">
                        <div class="form-group">
                            <label for="goals">Financial Goals:</label>
                            <!--<select class="form-control" ng-model="formData.goal" ng-init="formData.goal = goals[0]" ng-options="x for x in goals" ></select>-->
                            <select class="form-control" ng-model="formData.goal" required="">
                                <option ng-repeat="goal in goals" value="{{$index == 0 ? '':goal}}">{{goal}}</option>  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="time">Time Duration:</label>
                            <select class="form-control" ng-model="formData.time" required="">
                                <option ng-repeat="time in times" value="{{$index == 0 ? '':time}}">{{time}}</option>
                            </select>
                        </div>
                        <div class="form-group"> 
                            <label for="status">Current Status:</label>
                            <select class="form-control" ng-model="formData.status" required="">
                                <option ng-repeat="status in statuses" value="{{$index == 0 ? '':status}}">{{status}}</option>
                            </select>
                        </div> 
                        <!--<div class="form-group">
                            <label>Home Owner:</label>
                            <input type="radio"  ng-model="formData.owner" value="Yes" onchange="wakeupLib()">Yes
                            <input type="radio" ng-model="formData.owner" value="No" >No
                        </div>-->
                        <!--/Mini Form -->
                        <div ng-if="formData.owner == 'Yes'" class="row">
                            <div class="col-md-offset-1">
                                <div class="form-group">
                                    <label>What's your Interest Rate?</label>
                                    <input type="text" class="form-control percent" percent-number ng-model="formData.interest_rate" required="" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label>What's your current Loan Amount?</label>
                                    <input type="text" minlength="2" class="form-control loan-amount" ng-model="formData.loan_amount" required="" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label>Have you refinanced?</label>
                                    <input type="radio" ng-model="formData.refinaced" value="Yes" onchange="wakeupLib()" required="">Yes
                                    <input type="radio" ng-model="formData.refinaced" value="No" required="">No
                                </div>

                                <div class="form-group col-md-offset-1" ng-if="formData.refinaced == 'Yes'">
                                    <label>When did you Refinance?</label>
                                    <input type="text" class="form-control datepicker" ng-model="formData.refinacne_date" required="" />
                                </div>
                            </div>
                        </div>
                        <!--/Mini Form -->

                         
                        <div class="form-group">
                        <!-- <button type="button" class="btn btn-primary-outline pull-left btn-lg" ng-click="previousStep('1/signup')">Previous</button> -->
                        <button type="submit" ng-disabled="form.$invalid" ng-class="form.$invalid?'btn-default':'btn-primary'" class="btn pull-right btn-lg">Next</button>               
                        </div>
                        
                    </div>
                </form>	
            </div>	
            

            <div class="col-md-3 col-sm-3 col-xs-124">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                  Edit Personal Info
                </button>
            </div>

            
        </div>
    </div>
</section>
</div>





<script type="text/javascript">
    $(function () {
        $("#phone").mask("999-999-9999", {placeholder: "___-___-____"});
        $(".loan-amount").mask("$99,999,999", {placeholder: "10,000,000"});
    });

    /*var scope = angular.element($("#body")).scope()
    scope.loading = false;*/
</script> 