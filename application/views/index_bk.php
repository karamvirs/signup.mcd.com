<!DOCTYPE html>
<html lang="en" ng-app="stepA">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Non Profit CC</title>
        <!-- Bootstrap -->
        <!--<link rel="shortcut icon" type="image/png" href="<?= base_url() ?>/assets/favicon.ico"/>-->
        <link rel="stylesheet" href="<?= base_url() ?>/assets/datepicker/bootstrap-datepicker.css">
        <link href="<?= base_url() ?>/assets/theme/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/assets/theme/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/assets/theme/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/assets/theme/css/style.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link href="<?= base_url() ?>/assets/theme/css/responsive.css" rel="stylesheet" type="text/css" />	
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" />	
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .ng-clock{display: none !important;}
            .ng-clock.ng-scope{display: block !important;}
        </style>
        

            <!-- Main theme files -->
            <link href="../css/style.css" rel="stylesheet" type="text/css">
            <link href="../css/responsive.css" rel="stylesheet" type="text/css">   
    </head>
    <body ng-controller="page_load" class="ng-clock">
        <div class="main_wrapper transp-header">
            <header class="navbar navbar-static-top header fixed" id="top" role="slider">
<div class="container">
<div class="navbar-header">
<a href="http://37.60.244.107/~baacce5/dovly/" class="navbar-brand">
<img class="white-logo" src="../images/white-logo.png" alt="">
<img class="blue-logo" src="../images/blue-logo.png" alt=""></a> 
<button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> <span class="sr-only">Toggle navigation</span> 
<i class="ion-drag"></i>
</button>               
</div>
<nav class="collapse navbar-collapse" id="bs-navbar"> 
<ul class="main-menu nav navbar-nav navbar-right"> 
<!-- <li><a href="product-tour">Product Tour</a></li> -->
<li><a href="../improve-finances">Improve Finances</a></li>    
<li><a href="../partners">Partners</a></li>
<li><a href="../contact">Contact</a></li>
<div class="heder-links">
<!-- <a class="btn btn-started btn-primary" href="demo">Get a Demo</a> -->
<a class="btn btn-white-outline login-btn" href="https://msc.dovly.com/Home/SignIn">Sign In</a>
</div>
</ul>       
</nav>        
</div>
</header>
            <div  ng-if="page.alert && page.flash" class="alert alert-{{page.alert}} response-alert alert-dismissable" id="message">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><i class="fa fa-info-circle"></i></strong> <message>{{page.flash}}</message>
            </div>
            <ng-view></ng-view>
            <footer class="footer">
                <div class="container">
                <div class="social wow fadeIn">
                <h3>Let’s Get Connected</h3>
                <a href="https://www.facebook.com/dovly/" target="_blank"><i class="ion-social-facebook"></i> </a>
                <a href="https://twitter.com/dovly" target="_blank"><i class="ion-social-twitter"></i> </a>         
                <a href="https://plus.google.com/u/0/112253325432002916314" target="_blank"><i class="ion-social-googleplus"></i> </a>      
                <a href="https://www.youtube.com/channel/UC-PGiAyrJhmZFmfBDi1_xuw " target="_blank"><i class="ion-social-youtube"></i> </a>
                </div>  
                <div class="copyright wow fadeIn">
                <ul class="nav navbar-nav footer-nav">
                <li><a href="improve-credit">Improve Credit</a></li>    
                <li><a href="partners">Partners</a></li>
                <li><a href="demo">Get A Demo</a></li>
                <li><a href="contact">Contact</a></li>
                <li><a href="privacy" target="_blank">Privacy Policy</a></li> 
                <li><a href="terms-and-conditions" target="_blank">Terms &amp; Conditions</a></li>
                <li><a href="tel:302-365-0216">Call: 302-365-0216</a></li>
                </ul>
                <div class="clearfix"></div>
                    &copy; 2016 dovly. All Rights Reserved.  1910 Thomes Ave, Cheyenne, WY 82001
                    </div>
                    </div>
            </footer>
        </div>
        <!------ ===========  main_wrapper Close  ==========------>
        <!---------- Comingsoon  Popup Starts ----------->

        <div class="modal fade Comingsoon  plane-Popup" id="Comingsoon" tabindex="-1" role="dialog" aria-labelledby="ComingsoonLabel">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 

                    <div class="modal-body">	  
                        <h4 class="modal-title text-center">We're Launching Soon!</h4>
                        <p class=" text-center">Want to improve your financial future? Enter your name and email below to get exclusive access and updates about our launch!</p>
                        <div class="clearfix"></div><hr>

                        <form id="comingsoon-form" action="" method="" class="login_form common-form  ">

                            <div class="form-group">
                                <i class="ion-lock-combination"></i>
                                <input id="FirstName" name="FirstName" placeholder="First Name" class="form-control" type="text">
                            </div>
                            <div class="form-group">
                                <i class="ion-ios-email"></i>
                                <input id="email" name="Email" placeholder="Email Address" class="form-control" type="email">
                            </div>


                            <div class="form-group">
                                <button class="btn btn-primary btn-block btn-lg" type="submit">Submit</button>
                            </div>
                        </form>

                        <div class="clearfix"></div><br>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/wow.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/jquery.maskedinput.min.js"></script>
        <script src="<?= base_url() ?>/assets/theme/js/custom.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
        <script src="<?= base_url() . 'assets/datepicker/bootstrap-datepicker.min.js' ?>"></script>
        <script src="<?= base_url() . 'assets/wow.min.js' ?>"></script>
        <script src="https://js.stripe.com/v3/"></script>
        <script type="text/javascript">
            var apiUrl = "<?= $this->config->item('apiUrl') ?>";
            var token = "<?= $this->config->item('token') ?>";
            var customerId = "<?= @$_GET['id'] ? @$_GET['id'] : '' ?>";
            var mainPage = '/~baacce5/dovly/signup/index.php';
            var site_url = "<?= site_url() ?>/";
            customerName = '';
        </script>
        <script src="<?= base_url() . 'assets/main.js' ?>"></script>
        <script src="<?= base_url() . 'assets/mask.js' ?>"></script>
    </body>
</html>