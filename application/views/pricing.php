<!DOCTYPE html>
<!-- saved from url=(0044)https://dovly.com/pricing-template/ -->
<html class="js no-touchevents cssanimations csspointerevents csstransforms supports csstransforms3d preserve3d csstransitions js_active vc_desktop vc_transform gr__dovly_org" lang="en-US" prefix="og: https://ogp.me/ns#" style="">
   <!--<![endif]-->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>pricing-template - DOVLY</title>
      
      <!-- This site is optimized with the Yoast SEO plugin v5.8 - https://yoast.com/wordpress/plugins/seo/ -->
      <link rel="canonical" href="https://dovly.com/pricing-template/">
      <meta property="og:locale" content="en_US">
      <meta property="og:type" content="article">
      <meta property="og:title" content="pricing-template - DOVLY">
      <meta property="og:url" content="https://dovly.com/pricing-template/">
      <meta property="og:site_name" content="DOVLY">
      <meta name="twitter:card" content="summary">
      <meta name="twitter:title" content="pricing-template - DOVLY">
      <link rel="stylesheet" href="https://dovly.com/css/style.css" />
      <script async="" src="<?= base_url() ?>/assets/pricing-page-assets/analytics.js"></script><script src="<?= base_url() ?>/assets/pricing-page-assets/324085124767963" async=""></script><script src="<?= base_url() ?>/assets/pricing-page-assets/989276941174495" async=""></script><script async="" src="<?= base_url() ?>/assets/pricing-page-assets/fbevents.js"></script><script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/dovly.com\/","name":"DOVLY","potentialAction":{"@type":"SearchAction","target":"http:\/\/dovly.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
      <!-- / Yoast SEO plugin. -->
      <link rel="dns-prefetch" href="https://s0.wp.com/">
      <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
      <link rel="dns-prefetch" href="https://s.w.org/">
      <link rel="alternate" type="application/rss+xml" title="DOVLY » Feed" href="https://dovly.com/feed/">
      <link rel="alternate" type="application/rss+xml" title="DOVLY » Comments Feed" href="https://dovly.com/comments/feed/">
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/dovly.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.5"}};
         !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
      </script><script src="<?= base_url() ?>/assets/pricing-page-assets/wp-emoji-release.min.js" type="text/javascript" defer=""></script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel="stylesheet" id="lfb-reset-css" href="<?= base_url() ?>/assets/pricing-page-assets/reset.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="lfb-bootstrap-css" href="<?= base_url() ?>/assets/pricing-page-assets/bootstrap.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="lfb-bootstrap-select-css" href="<?= base_url() ?>/assets/pricing-page-assets/bootstrap-select.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="lfb-flat-ui-css" href="<?= base_url() ?>/assets/pricing-page-assets/flat-ui_frontend.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="lfb-colpick-css" href="<?= base_url() ?>/assets/pricing-page-assets/colpick.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="lfb-dropzone-css" href="<?= base_url() ?>/assets/pricing-page-assets/dropzone.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="lfb-estimationpopup-css" href="<?= base_url() ?>/assets/pricing-page-assets/lfb_forms.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="bookly-intlTelInput-css" href="<?= base_url() ?>/assets/pricing-page-assets/intlTelInput.css" type="text/css" media="all">
      <link rel="stylesheet" id="bookly-ladda-min-css" href="<?= base_url() ?>/assets/pricing-page-assets/ladda.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="bookly-picker-css" href="<?= base_url() ?>/assets/pricing-page-assets/picker.classic.css" type="text/css" media="all">
      <link rel="stylesheet" id="bookly-picker-date-css" href="<?= base_url() ?>/assets/pricing-page-assets/picker.classic.date.css" type="text/css" media="all">
      <link rel="stylesheet" id="bookly-main-css" href="<?= base_url() ?>/assets/pricing-page-assets/bookly-main.css" type="text/css" media="all">
      <link rel="stylesheet" id="bookly-customer-profile-css" href="<?= base_url() ?>/assets/pricing-page-assets/customer_profile.css" type="text/css" media="all">
      <link rel="stylesheet" id="cf7cf-style-css" href="<?= base_url() ?>/assets/pricing-page-assets/style.css" type="text/css" media="all">
      <link rel="stylesheet" id="essential-grid-plugin-settings-css" href="<?= base_url() ?>/assets/pricing-page-assets/settings.css" type="text/css" media="all">
      <link rel="stylesheet" id="tp-open-sans-css" href="<?= base_url() ?>/assets/pricing-page-assets/css" type="text/css" media="all">
      <link rel="stylesheet" id="tp-raleway-css" href="<?= base_url() ?>/assets/pricing-page-assets/css(1)" type="text/css" media="all">
      <link rel="stylesheet" id="tp-droid-serif-css" href="<?= base_url() ?>/assets/pricing-page-assets/css(2)" type="text/css" media="all">
      <link rel="stylesheet" id="tp-poppins-css" href="<?= base_url() ?>/assets/pricing-page-assets/css(3)" type="text/css" media="all">
      <link rel="stylesheet" id="tp-montserrat-css" href="<?= base_url() ?>/assets/pricing-page-assets/css(4)" type="text/css" media="all">
      <link rel="stylesheet" id="x-stack-css" href="<?= base_url() ?>/assets/pricing-page-assets/renew.css" type="text/css" media="all">
      <link rel="stylesheet" id="x-google-fonts-css" href="<?= base_url() ?>/assets/pricing-page-assets/css(5)" type="text/css" media="all">
      <link rel="stylesheet" id="js_composer_front-css" href="<?= base_url() ?>/assets/pricing-page-assets/js_composer.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="rtbs-css" href="<?= base_url() ?>/assets/pricing-page-assets/rtbs_style.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="smile-modal-style-css" href="<?= base_url() ?>/assets/pricing-page-assets/modal.min.css" type="text/css" media="all">
      <link rel="stylesheet" id="jetpack_css-css" href="<?= base_url() ?>/assets/pricing-page-assets/jetpack.css" type="text/css" media="all">
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/jquery.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/jquery-migrate.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/core.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/widget.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/position.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/tooltip.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/mouse.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/slider.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/datepicker.min.js"></script>
      <script type="text/javascript">
         jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
      </script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/jquery.ui.touch-punch.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/bootstrap-switch.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/bootstrap-select.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/colpick.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/dropzone.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/jquery-ui-i18n.min.js"></script>
      <script type="text/javascript">
         /* <![CDATA[ */
         var wpe_forms = [{"currentRef":0,"ajaxurl":"http:\/\/dovly.com\/wp-admin\/admin-ajax.php","initialPrice":"1000","max_price":"10000","percentToPay":"100","currency":"$","currencyPosition":"left","intro_enabled":"1","save_to_cart":"0","colorA":"#1abc9c","close_url":"#","animationsSpeed":"0.5","email_toUser":"1","showSteps":"0","formID":"1","gravityFormID":"0","showInitialPrice":"0","disableTipMobile":"0","legalNoticeEnable":"0","links":[],"redirections":[],"useRedirectionConditions":"0","usePdf":0,"txt_yes":"Yes","txt_no":"No","txt_lastBtn":"Let's Get Started!","txt_btnStep":"NEXT STEP","dateFormat":"MM d, yy","datePickerLanguage":"","thousandsSeparator":",","decimalsSeparator":".","millionSeparator":"","summary_hideQt":"0","summary_hideZero":"0","summary_hidePrices":"0","groupAutoClick":"1","filesUpload_text":"Drop files here to upload","filesUploadSize_text":"File is too big (max size: {{maxFilesize}}MB)","filesUploadType_text":"Invalid file type","filesUploadLimit_text":"You can not upload any more files","sendContactASAP":"0","showTotalBottom":"1","stripePubKey":"","scrollTopMargin":"0","redirectionDelay":"5","gmap_key":"","txtDistanceError":"Calculating the distance could not be performed, please verify the input addresses","captchaUrl":"http:\/\/dovly.com\/wp-content\/plugins\/WP_Estimation_Form\/includes\/captcha\/get_captcha.php","summary_noDecimals":"0","scrollTopPage":"0"}];
         /* ]]> */
      </script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/lfb_form.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/lfb_frontend.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/spin.min.js"></script>
      <style type="text/css"></style>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/ladda.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/hammer.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/jquery.hammer.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/picker.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/picker.date.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/intlTelInput.min.js"></script>
      <script type="text/javascript">
         /* <![CDATA[ */
         var BooklyL10n = {"today":"Today","months":["January","February","March","April","May","June","July","August","September","October","November","December"],"days":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"daysShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"nextMonth":"Next month","prevMonth":"Previous month","show_more":"Show more"};
         /* ]]> */
      </script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/bookly.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/customer_profile.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/x-head.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/cs-head.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/rtbs.min.js"></script>
      <link rel="https://api.w.org/" href="https://dovly.com/wp-json/">
      <link rel="shortlink" href="https://wp.me/P8w6CG-9u">
      <link rel="alternate" type="application/json+oembed" href="https://dovly.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdovly.com%2Fpricing-template%2F">
      <link rel="alternate" type="text/xml+oembed" href="https://dovly.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdovly.com%2Fpricing-template%2F&amp;format=xml">
      <!-- Facebook Pixel Code -->
      <script>
         !function(f,b,e,v,n,t,s)
         {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};
         if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
         n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t,s)}(window,document,'script',
         'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '324085124767963'); 
         fbq('track', 'PageView');
      </script>
      <noscript>
         <img height="1" width="1" 
            src="https://www.facebook.com/tr?id=324085124767963&ev=PageView
            &noscript=1"/>
      </noscript>
      <!-- End Facebook Pixel Code -->
      <link rel="dns-prefetch" href="https://v0.wordpress.com/">
      <style type="text/css">img#wpstats{display:none}</style>
      <!--[if lte IE 9]>
      <link rel="stylesheet" type="text/css" href="https://dovly.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen">
      <![endif]-->
      <style>
         @import url(https://fonts.googleapis.com/css?family=Lato:400,700);body:not(.wp-admin) #estimation_popup.wpe_bootstraped[data-form="1"] { font-family:"Lato"; }#estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel { background-color:#ecf0f1; }#estimation_popup.wpe_bootstraped[data-form="1"] #lfb_loader { background-color:#1abc9c; }#estimation_popup.wpe_bootstraped[data-form="1"]  { color:#34495e; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .genSlide .lfb_totalBottomContainer hr  { border-color:#bdc3c7; }
         body #estimation_popup.wpe_bootstraped[data-form="1"] .form-control,#estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel ,#estimation_popup.wpe_bootstraped[data-form="1"] p,#estimation_popup.wpe_bootstraped[data-form="1"] #lfb_summary tbody td,#estimation_popup.wpe_bootstraped[data-form="1"] #lfb_summary tbody #sfb_summaryTotalTr th:not(#lfb_summaryTotal) { color:#bdc3c7; }
         #estimation_popup.wpe_bootstraped[data-form="1"]  .tooltip .tooltip-inner,#estimation_popup.wpe_bootstraped[data-form="1"]   #mainPanel .genSlide .genContent div.selectable span.icon_quantity,#estimation_popup.wpe_bootstraped[data-form="1"]   .dropdown-inverse { background-color:#34495e; }
         #estimation_popup.wpe_bootstraped[data-form="1"]   .tooltip.top .tooltip-arrow { border-top-color:#34495e; }
         #estimation_popup.wpe_bootstraped[data-form="1"]   .tooltip.bottom .tooltip-arrow { border-bottom-color:#34495e; }
         #estimation_popup.wpe_bootstraped[data-form="1"]   .btn-primary,#estimation_popup.wpe_bootstraped[data-form="1"] .gform_button,#estimation_popup.wpe_bootstraped[data-form="1"]   .btn-primary:hover,#estimation_popup.wpe_bootstraped[data-form="1"]   .btn-primary:active,#estimation_popup.wpe_bootstraped[data-form="1"]    .genPrice .progress .progress-bar-price,#estimation_popup.wpe_bootstraped[data-form="1"]    .progress-bar,#estimation_popup.wpe_bootstraped[data-form="1"]   .quantityBtns a,#estimation_popup.wpe_bootstraped[data-form="1"]   .btn-primary:active,#estimation_popup.wpe_bootstraped[data-form="1"]    .btn-primary.active,#estimation_popup.wpe_bootstraped[data-form="1"]    .open .dropdown-toggle.btn-primary,#estimation_popup.wpe_bootstraped[data-form="1"]   .dropdown-inverse li.active > a,#estimation_popup.wpe_bootstraped[data-form="1"]    .dropdown-inverse li.selected > a,#estimation_popup.wpe_bootstraped[data-form="1"]   .btn-primary:active,#estimation_popup.wpe_bootstraped[data-form="1"]
         .btn-primary.active,#estimation_popup.wpe_bootstraped[data-form="1"]   .open .dropdown-toggle.btn-primary,#estimation_popup.wpe_bootstraped[data-form="1"]   .btn-primary:hover,#estimation_popup.wpe_bootstraped[data-form="1"]    .btn-primary:focus,#estimation_popup.wpe_bootstraped[data-form="1"]    .btn-primary:active,#estimation_popup.wpe_bootstraped[data-form="1"]    .btn-primary.active,#estimation_popup.wpe_bootstraped[data-form="1"]    .open .dropdown-toggle.btn-primary { background-color:#1abc9c; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .lfb_dropzone:focus,#estimation_popup.wpe_bootstraped[data-form="1"] .has-switch > div.switch-on label,#estimation_popup.wpe_bootstraped[data-form="1"]   .form-group.focus .form-control,#estimation_popup.wpe_bootstraped[data-form="1"]   .form-control:focus { border-color:#1abc9c; }
         #estimation_popup.wpe_bootstraped[data-form="1"] a:not(.btn),#estimation_popup.wpe_bootstraped[data-form="1"]   a:not(.btn):hover,#estimation_popup.wpe_bootstraped[data-form="1"]   a:not(.btn):active,#estimation_popup.wpe_bootstraped[data-form="1"]   #mainPanel .genSlide .genContent div.selectable.checked span.icon_select,#estimation_popup.wpe_bootstraped[data-form="1"]   #mainPanel #finalPrice,#estimation_popup.wpe_bootstraped[data-form="1"]    .ginput_product_price,#estimation_popup.wpe_bootstraped[data-form="1"]   .checkbox.checked,#estimation_popup.wpe_bootstraped[data-form="1"]    .radio.checked,#estimation_popup.wpe_bootstraped[data-form="1"]   .checkbox.checked .second-icon,#estimation_popup.wpe_bootstraped[data-form="1"]    .radio.checked .second-icon { color:#1abc9c; }
         #estimation_popup.wpe_bootstraped[data-form="1"]   #mainPanel .genSlide .genContent div.selectable .img { max-width:64px;  max-height:64px; }
         #estimation_popup.wpe_bootstraped[data-form="1"]   #mainPanel,#estimation_popup.wpe_bootstraped[data-form="1"]   .form-control { color:#bdc3c7; }
         #estimation_popup.wpe_bootstraped[data-form="1"]   .form-control,#estimation_popup.wpe_bootstraped[data-form="1"] .lfb_dropzone  { color:#bdc3c7;  border-color:#bdc3c7; }
         #estimation_popup.wpe_bootstraped[data-form="1"]  .lfb_dropzone .dz-preview .dz-remove { color:#bdc3c7; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .btn-default,#estimation_popup.wpe_bootstraped[data-form="1"] .has-switch span.switch-right,#estimation_popup.wpe_bootstraped[data-form="1"] .bootstrap-datetimepicker-widget .has-switch span.switch-right,#estimation_popup.wpe_bootstraped[data-form="1"] .dropdown-menu { background-color:#bdc3c7;  color:#ffffff; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .lfb_bootstrap-select.btn-group .dropdown-menu li a{ color:#ffffff; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .lfb_bootstrap-select.btn-group .dropdown-menu li.selected> a,#estimation_popup.wpe_bootstraped[data-form="1"] .lfb_bootstrap-select.btn-group .dropdown-menu li.selected> a:hover{ background-color:#1abc9c; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .has-switch>div.switch-off label{ border-color:#bdc3c7;  background-color:#7f8c9a; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .has-switch>div.switch-on label{ background-color:#bdc3c7; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .btn-default .bs-caret > .caret {  border-bottom-color:#ffffff;   border-top-color:#ffffff; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .genPrice .progress .progress-bar-price  { font-size:18px; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .itemDes  { max-width:240px; }
         #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel .genSlide .genContent div.selectable .wpe_itemQtField  { width:64px; }
         #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel .genSlide .genContent div.selectable .wpe_itemQtField .wpe_qtfield  { margin-left:-18px; }
         body .lfb_datepickerContainer .ui-datepicker-title {  background-color:#1abc9c; }
         body .lfb_datepickerContainer td a { color:#1abc9c; }
         body .lfb_datepickerContainer  td.ui-datepicker-today a { color:#34495e; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .has-switch span.switch-left { background-color:#1abc9c; }
         #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel #lfb_summary table thead { background-color:#1abc9c; }
         #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel #lfb_summary table th.sfb_summaryStep { background-color:#bdc3c7; }
         #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel #lfb_summary table #lfb_summaryTotal { color:#1abc9c; }
         #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel .wpe_sliderQt { background-color:#bdc3c7; }#estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel [data-type="slider"] { background-color:#bdc3c7; }#estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel .wpe_sliderQt .ui-slider-range, #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel .wpe_sliderQt .ui-slider-handle,  #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel [data-type="slider"] .ui-slider-range,#estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel [data-type="slider"] .ui-slider-handle { background-color:#1abc9c ; }
         #estimation_popup.wpe_bootstraped[data-form="1"] #mainPanel #finalPrice span:nth-child(2) { color:#bdc3c7; }
         #estimation_popup.wpe_bootstraped[data-form="1"] .lfb_colorPreview { border-color:#bdc3c7; }
         #lfb_bootstraped.lfb_bootstraped[data-form="1"] #estimation_popup[data-previousstepbtn="true"] .linkPrevious { background-color:#bdc3c7;  color:#ffffff; }
      </style>
      <link rel="icon" href="https://dovly.com/wp-content/uploads/2016/10/cropped-favicon-1-32x32.jpg" sizes="32x32">
      <link rel="icon" href="https://dovly.com/wp-content/uploads/2016/10/cropped-favicon-1-192x192.jpg" sizes="192x192">
      <link rel="apple-touch-icon-precomposed" href="https://dovly.com/wp-content/uploads/2016/10/cropped-favicon-1-180x180.jpg">
      <meta name="msapplication-TileImage" content="https://dovly.com/wp-content/uploads/2016/10/cropped-favicon-1-270x270.jpg">
      <link rel="stylesheet" type="text/css" id="wp-custom-css" href="<?= base_url() ?>/assets/pricing-page-assets/saved_resource.css">
      <style type="text/css" data-type="vc_custom-css">.x-container.offset { margin-top: 0;}</style>
      <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1522320241696{margin-top: 0px !important;margin-right: 0px !important;margin-bottom: 0px !important;margin-left: 0px !important;background-color: #00a957 !important;}.vc_custom_1522662435510{margin-top: 0px !important;margin-right: 0px !important;margin-bottom: 0px !important;margin-left: 0px !important;}.vc_custom_1522219494056{margin-top: 0px !important;margin-right: 0px !important;margin-bottom: 0px !important;margin-left: 0px !important;}</style>
      <noscript>
         <style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>
      <style id="x-generated-css" type="text/css">a,h1 a:hover,h2 a:hover,h3 a:hover,h4 a:hover,h5 a:hover,h6 a:hover,.x-comment-time:hover,#reply-title small a,.comment-reply-link:hover,.x-comment-author a:hover,.x-recent-posts a:hover .h-recent-posts{color:#fa0505;}a:hover,#reply-title small a:hover{color:#222222;}.entry-title:before{color:#ddd;}a.x-img-thumbnail:hover,li.bypostauthor > article.comment{border-color:#fa0505;}.flex-direction-nav a,.flex-control-nav a:hover,.flex-control-nav a.flex-active,.x-dropcap,.x-skill-bar .bar,.x-pricing-column.featured h2,.h-comments-title small,.x-pagination a:hover,.x-entry-share .x-share:hover,.entry-thumb,.widget_tag_cloud .tagcloud a:hover,.widget_product_tag_cloud .tagcloud a:hover,.x-highlight,.x-recent-posts .x-recent-posts-img:after,.x-portfolio-filters{background-color:#fa0505;}.x-portfolio-filters:hover{background-color:#222222;}.x-main{width:76.79803%;}.x-sidebar{width:16.79803%;}.x-topbar .p-info,.x-topbar .p-info a,.x-topbar .x-social-global a{color:#ffffff;}.x-topbar .p-info a:hover{color:#959baf;}.x-topbar{background-color:#2980b9;}.x-navbar .desktop .x-nav > li:before{padding-top:42px;}.x-navbar .desktop .x-nav > li > a,.x-navbar .desktop .sub-menu li > a,.x-navbar .mobile .x-nav li a{color:hsl(0,0%,69%);}.x-navbar .desktop .x-nav > li > a:hover,.x-navbar .desktop .x-nav > .x-active > a,.x-navbar .desktop .x-nav > .current-menu-item > a,.x-navbar .desktop .sub-menu li > a:hover,.x-navbar .desktop .sub-menu li.x-active > a,.x-navbar .desktop .sub-menu li.current-menu-item > a,.x-navbar .desktop .x-nav .x-megamenu > .sub-menu > li > a,.x-navbar .mobile .x-nav li > a:hover,.x-navbar .mobile .x-nav li.x-active > a,.x-navbar .mobile .x-nav li.current-menu-item > a{color:hsl(0,96%,50%);}.x-btn-navbar,.x-btn-navbar:hover{color:#ffffff;}.x-navbar .desktop .sub-menu li:before,.x-navbar .desktop .sub-menu li:after{background-color:hsl(0,0%,69%);}.x-navbar,.x-navbar .sub-menu{background-color:rgb(255,255,255) !important;}.x-btn-navbar,.x-btn-navbar.collapsed:hover{background-color:#476481;}.x-btn-navbar.collapsed{background-color:#2980b9;}.x-navbar .desktop .x-nav > li > a:hover > span,.x-navbar .desktop .x-nav > li.x-active > a > span,.x-navbar .desktop .x-nav > li.current-menu-item > a > span{box-shadow:0 2px 0 0 hsl(0,96%,50%);}.x-navbar .desktop .x-nav > li > a{height:px;padding-top:42px;}.x-navbar .desktop .x-nav > li ul{top:px;}.x-colophon.bottom{background-color:#183966;}.x-colophon.bottom,.x-colophon.bottom a,.x-colophon.bottom .x-social-global a{color:#ffffff;}.h-landmark{font-weight:300;}.x-comment-author a{color:#333333;}.x-comment-author a,.comment-form-author label,.comment-form-email label,.comment-form-url label,.comment-form-rating label,.comment-form-comment label,.widget_calendar #wp-calendar caption,.widget_calendar #wp-calendar th,.x-accordion-heading .x-accordion-toggle,.x-nav-tabs > li > a:hover,.x-nav-tabs > .active > a,.x-nav-tabs > .active > a:hover{color:#02aed4;}.widget_calendar #wp-calendar th{border-bottom-color:#02aed4;}.x-pagination span.current,.x-portfolio-filters-menu,.widget_tag_cloud .tagcloud a,.h-feature-headline span i,.widget_price_filter .ui-slider .ui-slider-handle{background-color:#02aed4;}@media (max-width:979px){}body{font-size:16px;font-style:normal;font-weight:300;color:#333333;background:#ffffff url(//theme.co/media/subtlenet2.png) center top repeat;}a:focus,select:focus,input[type="file"]:focus,input[type="radio"]:focus,input[type="submit"]:focus,input[type="checkbox"]:focus{outline:thin dotted #333;outline:5px auto #fa0505;outline-offset:-1px;}h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{font-family:"Montserrat",sans-serif;font-style:normal;font-weight:400;}h1,.h1{letter-spacing:0em;}h2,.h2{letter-spacing:0em;}h3,.h3{letter-spacing:0em;}h4,.h4{letter-spacing:0em;}h5,.h5{letter-spacing:0em;}h6,.h6{letter-spacing:0em;}.w-h{font-weight:400 !important;}.x-container.width{width:88%;}.x-container.max{max-width:1170px;}.x-main.full{float:none;display:block;width:auto;}@media (max-width:979px){.x-main.full,.x-main.left,.x-main.right,.x-sidebar.left,.x-sidebar.right{float:none;display:block;width:auto !important;}}.entry-header,.entry-content{font-size:16px;}body,input,button,select,textarea{font-family:"Poppins",sans-serif;}h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,h1 a,h2 a,h3 a,h4 a,h5 a,h6 a,.h1 a,.h2 a,.h3 a,.h4 a,.h5 a,.h6 a,blockquote{color:#02aed4;}.cfc-h-tx{color:#02aed4 !important;}.cfc-h-bd{border-color:#02aed4 !important;}.cfc-h-bg{background-color:#02aed4 !important;}.cfc-b-tx{color:#333333 !important;}.cfc-b-bd{border-color:#333333 !important;}.cfc-b-bg{background-color:#333333 !important;}.x-navbar-inner{min-height:px;}.x-brand{margin-top:10px;font-family:"Oswald",sans-serif;font-size:54px;font-style:normal;font-weight:700;letter-spacing:0em;text-transform:uppercase;color:#272727;}.x-brand:hover,.x-brand:focus{color:#272727;}.x-navbar .x-nav-wrap .x-nav > li > a{font-family:"Poppins",sans-serif;font-style:normal;font-weight:500;letter-spacing:0em;}.x-navbar .desktop .x-nav > li > a{font-size:16px;}.x-navbar .desktop .x-nav > li > a:not(.x-btn-navbar-woocommerce){padding-left:15px;padding-right:15px;}.x-navbar .desktop .x-nav > li > a > span{margin-right:-0em;}.x-btn-navbar{margin-top:19px;}.x-btn-navbar,.x-btn-navbar.collapsed{font-size:24px;}@media (max-width:979px){.x-widgetbar{left:0;right:0;}}.x-btn,.button,[type="submit"]{color:#fff;border-color:#c10b03;background-color:#c10b03;text-shadow:0 0.075em 0.075em rgba(0,0,0,0.5);}.x-btn:hover,.button:hover,[type="submit"]:hover{color:#fff;border-color:#222222;background-color:#222222;text-shadow:0 0.075em 0.075em rgba(0,0,0,0.5);}.x-btn.x-btn-real,.x-btn.x-btn-real:hover{margin-bottom:0.25em;text-shadow:0 0.075em 0.075em rgba(0,0,0,0.65);}.x-btn.x-btn-real{box-shadow:0 0.25em 0 0 #f39c12,0 4px 9px rgba(0,0,0,0.75);}.x-btn.x-btn-real:hover{box-shadow:0 0.25em 0 0 #f39c12,0 4px 9px rgba(0,0,0,0.75);}.x-btn.x-btn-flat,.x-btn.x-btn-flat:hover{margin-bottom:0;text-shadow:0 0.075em 0.075em rgba(0,0,0,0.65);box-shadow:none;}.x-btn.x-btn-transparent,.x-btn.x-btn-transparent:hover{margin-bottom:0;border-width:3px;text-shadow:none;text-transform:uppercase;background-color:transparent;box-shadow:none;}</style>
      <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         
         ga('create', 'UA-86492179-1', 'auto');
         ga('send', 'pageview');
         
      </script><script async="true" type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/roundtrip.js"></script><script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/Q4CKIHBEYFFW5AA6WZHC74"></script><script async="true" type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/EQQNBYOY3NHXVMKJLSIUCZ"></script>
      <div style="width: 1px; height: 1px; display: inline; position: absolute;"><img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out"><img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(1)"><img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(2)"><img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(3)"><img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(4)"></div>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/sendrolling.js"></script>
      <div style="width: 1px; height: 1px; display: inline; position: absolute;"><img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(5)">
         <img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(6)">
         <img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(7)">
         <img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(8)">
         <img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(9)">
         <img height="1" width="1" style="border-style:none;" alt="" src="<?= base_url() ?>/assets/pricing-page-assets/out(10)">
      </div>
      <script style="display: none;">var tvt = tvt || {}; tvt.captureVariables = function(a){for(var b=
         new Date,c={},d=Object.keys(a||{}),e=0,f;f=d[e];e++)if(a.hasOwnProperty(f)&&"undefined"!=typeof a[f])try{var g=[];c[f]=JSON.stringify(a[f],function(a,b){try{if("function"!==typeof b){if("object"===typeof b&&null!==b){if(b instanceof HTMLElement||b instanceof Node||-1!=g.indexOf(b))return;g.push(b)}return b}}catch(H){}})}catch(l){}a=document.createEvent("CustomEvent");a.initCustomEvent("TvtRetrievedVariablesEvent",!0,!0,{variables:c,date:b});window.dispatchEvent(a)};window.setTimeout(function() {tvt.captureVariables({'dataLayer.hide': (function(a){a=a.split(".");for(var b=window,c=0;c<a.length&&(b=b[a[c]],b);c++);return b})('dataLayer.hide'),'gaData': window['gaData'],'dataLayer': window['dataLayer']})}, 2000);
      </script>
   </head>
   <body class="page-template-default page page-id-588 x-renew x-navbar-static-active x-full-width-layout-active x-full-width-active x-page-title-disabled wpb-js-composer js-comp-ver-5.4.7 vc_responsive x-v4_6_4 cornerstone-v1_3_3" data-gr-c-s-loaded="true">
      <div id="top" class="site">
         
         <header class="navbar navbar-static-top header fixed" id="top" role="slider">
<div class="container">
<div class="navbar-header">
<a href="https://dovly.com" class="navbar-brand">
<img class="white-logo" src="https://dovly.com/images/white-logo.png" alt="">
<img class="blue-logo" src="https://dovly.com/images/blue-logo.png" alt=""></a> 
<button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> <span class="sr-only">Toggle navigation</span> 
<i class="ion-drag"></i>
</button>               
</div>
<nav class="collapse navbar-collapse" id="bs-navbar"> 
<ul class="main-menu nav navbar-nav navbar-right"> 
<!-- <li><a href="product-tour">Product Tour</a></li> -->
<li><a href="<?php echo $this->config->item('main_site_url'); ?>/improve-finances">Improve Finances</a></li>    
<li><a href="<?php echo $this->config->item('main_site_url'); ?>/partners">Partners</a></li>
<li><a href="<?php echo $this->config->item('main_site_url'); ?>/contact">Contact</a></li>
<div class="heder-links">
<!-- <a class="btn btn-started btn-primary" href="demo">Get a Demo</a> -->
<a class="btn btn-white-outline login-btn" href="https://dovly.mycreditdash.com">Sign In</a>
</div>
</ul>       
</nav>        
</div>
</header>
         
         
         <div class="x-container max width offset">
            <div class="x-main full" role="main">
               <article id="post-588" class="post-588 page type-page status-publish hentry no-post-thumbnail">
                  <div class="entry-wrap">
                     <div class="entry-content content">
                        <section data-vc-full-width="true" data-vc-full-width-init="true" class="vc_section pricing-section vc_custom_1522320241696 vc_section-has-fill" style="position: relative; left: -88.5px; box-sizing: border-box; width: 1347px; padding-left: 88.5px; padding-right: 88.5px;">
                           <div id="x-content-band-1" class="x-content-band vc" style="background-color: transparent; padding-top: 0px; padding-bottom: 0px;">
                              <div class="x-container wpb_row">
                                 <div class="x-column x-sm vc x-1-1" style="">
                                    <h4 class="h-custom-headline center-text"><span><span style="color: #ffffff;">Our Pricing Plans</span></span></h4>
                                    <!--<p style="text-align: center;"><span style="color: #fff;">No SetUp Fees, Your Monthly Donation is Tax Deductible</span></p>-->
                                    <?php  if(isset($_GET['err'])) { ?>
                                    <p style="text-align: center;"><span style="color: yellow;font-size:20px;">Please select a plan first.</span></p>
                                    <?php } ?>
                                 </div>
                              </div>
                           </div>
                           <div id="x-content-band-2" class="x-content-band vc pricing-container" style="background-color: transparent; padding-top: 0px; padding-bottom: 0px;">
                              <div class="x-container max width wpb_row">
                                 
                                 <div class="x-column x-sm vc pricing-box x-1-2" style="">
                                    <h4 class="h-custom-headline plan-name center-text"><span>1 Bureau</span></h4>
                                    <h2 class="h-custom-headline price center-text"><span><strong>$</strong>39<strong>/month</strong></span></h2>
                                    <!--<h4 class="h-custom-headline tag-line center-text" style="border-top:0"><span style="font-size:17px">Ideal for incomes between $3k-$4k a month</span></h4>-->
                                    <div class="vc_btn3-container vc_btn3-center">
                                       <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-flat vc_btn3-color-danger" href="<?= base_url('/?p=1#/step/1/signup') ?>" title="">Get Started</a>
                                    </div>
                                    <ul class="x-ul-icons">
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>Unlimited Credit Disputes</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>1 Bureau Credit Monitoring</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>Access to Credit Score</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>24/7 Credit Portal Access</li>

                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>Chat &amp; Email Support</li>

                                    </ul>
                                 </div>
                                 <div class="x-column x-sm vc pricing-box x-1-2" style="">
                                    <h4 class="h-custom-headline plan-name center-text"><span>3 Bureau</span></h4>
                                    <h2 class="h-custom-headline price center-text"><span><strong>$</strong>49<strong>/month</strong></span></h2>
                                    <!--<h4 class="h-custom-headline tag-line center-text" style="border-top:0"><span  style="font-size:17px;border-top:0">Designed for people earning more than $5k a month</span></h4>-->
                                    <div class="vc_btn3-container vc_btn3-center">
                                       <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-flat vc_btn3-color-danger" href="<?= base_url('/?p=2#/step/1/signup') ?>" title="">Get Started</a>
                                    </div>
                                    <ul class="x-ul-icons">
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>Unlimited Credit Disputes</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>3 Bureau Credit Monitoring</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>Access to All 3 Credit Scores</li>

                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>24/7 Credit Portal Access</li>

                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>Chat &amp; Email Support</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <section data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_section vc_custom_1522662435510" style="position: relative; left: -88.5px; box-sizing: border-box; width: 1347px;">
                           <div id="x-content-band-3" class="x-content-band vc pricing-features-section" style="background-color: transparent; padding-top: 0px; padding-bottom: 0px;">
                              <div class="x-container wpb_row" style="display:none">
                                 <div class="x-column x-sm vc x-1-1" style="padding-bottom:30px;">
                                    <h4 class="h-custom-headline">
                                       <span>
                                          <p style="text-align: center;"><span style="color: #000000;">Heres what sets us apart from the rest!</span></p>
                                       </span>
                                    </h4>
                                 </div>
                                 <div class="x-column x-sm vc" style="">
                                    <hr class="x-clear">
                                 </div>
                                 <div class="x-column x-sm vc x-1-2" style=""><img class="x-img right" src="<?= base_url() ?>/assets/pricing-page-assets/nppc-credit-repair.jpg"></div>
                                 <div class="x-column x-sm vc features-box x-1-2" style="">
                                    <ul class="x-ul-icons">
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>We don’t just monitor your credit, we help rebuild it!</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>We are usually done in less time than those large credit repair companies</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>We don’t treat you like a number, you’re part of the DOVLY family</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>Your monthly donation is tax deductible</li>
                                       <li class="x-li-icon"><i class="x-icon-" data-x-icon="" aria-hidden="true"></i>We offer Military discounts</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <span class="cp-load-after-post"></span>  
                     </div>
                  </div>
               </article>
            </div>
         </div>
         <script>
            jQuery( function($) {
            
            $(document).on('click', 'li.vc_tta-tab a', function( e ){
            $('html, body').stop();
            });
            $(document).on('click', 'li', function( e ){
            $('html, body').stop();
            });
            $(document).on('click', '.vc_tta-panel-title', function( e ){
            $('html, body').stop();
            });
            });  
         </script>  
         <a class="x-scroll-top right fade" href="https://dovly.com/pricing-template/#top" title="Back to Top">
         <i class="x-icon-angle-up" data-x-icon=""></i>
         </a>
         <script>
            jQuery(document).ready(function($) {
            
              var windowObj            = $(window);
              var body                 = $('body');
              var bodyOffsetBottom     = windowObj.scrollBottom();             // 1
              var bodyHeightAdjustment = body.height() - bodyOffsetBottom;     // 2
              var bodyHeightAdjusted   = body.height() - bodyHeightAdjustment; // 3
              var scrollTopAnchor      = $('.x-scroll-top');
            
              function sizingUpdate(){
                var bodyOffsetTop = windowObj.scrollTop();
                if ( bodyOffsetTop > ( bodyHeightAdjusted * 0.75 ) ) {
                  scrollTopAnchor.addClass('in');
                } else {
                  scrollTopAnchor.removeClass('in');
                }
              }
            
              windowObj.bind('scroll', sizingUpdate).resize(sizingUpdate);
              sizingUpdate();
            
              scrollTopAnchor.click(function(){
                $('html,body').animate({ scrollTop: 0 }, 850, 'easeInOutExpo');
                return false;
              });
            
            });
            
         </script>
         
         <footer class="footer">
                <div class="container">
                <div class="social wow fadeIn">
                <h3>Let’s Get Connected</h3>
                <a href="https://www.facebook.com/dovly/" target="_blank"><i class="ion-social-facebook"></i> </a>
                <a href="https://twitter.com/dovly" target="_blank"><i class="ion-social-twitter"></i> </a>         
                <a href="https://plus.google.com/u/0/112253325432002916314" target="_blank"><i class="ion-social-googleplus"></i> </a>      
                <a href="https://www.youtube.com/channel/UC-PGiAyrJhmZFmfBDi1_xuw " target="_blank"><i class="ion-social-youtube"></i> </a>
                </div>  
                <div class="copyright wow fadeIn">
                <!--<ul class="nav navbar-nav footer-nav">
                <li><a href="improve-credit">Improve Credit</a></li>    
                <li><a href="partners">Partners</a></li>
                <li><a href="demo">Get A Demo</a></li>
                <li><a href="contact">Contact</a></li>
                <li><a href="privacy" target="_blank">Privacy Policy</a></li> 
                <li><a href="terms-and-conditions" target="_blank">Terms &amp; Conditions</a></li>
                <li><a href="tel:302-365-0216">Call: 302-365-0216</a></li>
                </ul>-->
                <div class="clearfix"></div>
                    &copy; 2016 dovly. All Rights Reserved.  1910 Thomes Ave, Cheyenne, WY 82001
                    </div>
                    </div>
            </footer>
         
         
      </div>
      <!-- END #top.site -->
      <script type="text/javascript" id="modal">
         jQuery(window).on( 'load', function(){
            startclock();
         });
         function stopclock (){
           if(timerRunning) clearTimeout(timerID);
           timerRunning = false;
           document.cookie="time=0";
         }
         function showtime () {
           var now = new Date();
           var my = now.getTime() ;
           now = new Date(my-diffms) ;
           document.cookie="time="+now.toLocaleString();
           timerID = setTimeout('showtime()',10000);
           timerRunning = true;
         }
         function startclock () {
           stopclock();
           showtime();
         }
         var timerID = null;
         var timerRunning = false;
         var x = new Date() ;
         var now = x.getTime() ;
         var gmt = 1526634881 * 1000 ;
         var diffms = (now - gmt) ;
      </script>
      <div style="display:none">
      </div>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/devicepx-jetpack.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/x-body.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/comment-reply.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/cs-body.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/wp-embed.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/js_composer_front.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>/assets/pricing-page-assets/e-201820.js" async="" defer=""></script>
      <script type="text/javascript">
         _stq = window._stq || [];
         _stq.push([ 'view', {v:'ext',j:'1:5.5',blog:'125862646',post:'588',tz:'-7',srv:'dovly.com'} ]);
         _stq.push([ 'clickTrackerInit', '125862646', '588' ]);
      </script>
      <script>
         jQuery(document).ready(function(){
         jQuery(".hear-about-us select option:first:contains('---')").html('How did you hear about us?');
         }); 
      </script>
      <script type="text/javascript">
         adroll_adv_id = "Q4CKIHBEYFFW5AA6WZHC74";
         adroll_pix_id = "EQQNBYOY3NHXVMKJLSIUCZ";
         /* OPTIONAL: provide email to improve user identification */
         /* adroll_email = "username@example.com"; */
         (function () {
             var _onload = function(){
                 if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
                 if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
                 var scr = document.createElement("script");
                 var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "https://a.adroll.com");
                 scr.setAttribute('async', 'true');
                 scr.type = "text/javascript";
                 scr.src = host + "/j/roundtrip.js";
                 ((document.getElementsByTagName('head') || [null])[0] ||
                     document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
             };
             if (window.addEventListener) {window.addEventListener('load', _onload, false);}
             else {window.attachEvent('onload', _onload)}
         }());
      </script>
      <img src="<?= base_url() ?>/assets/pricing-page-assets/g.gif" alt=":)" width="6" height="5" id="wpstats">
   </body>
</html>