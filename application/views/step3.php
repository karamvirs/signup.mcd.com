<div  ng-controller="ctrlStep3">
    <div  ng-if="scrollup" class="scrollup alert alert-info response-alert alert-dismissible" data-auto-dismiss>
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><i class="fa fa-info-circle"></i></strong> <message>Please scroll up</message>
    </div>
<div class="page_title about-title"  ng-hide="loading">
    <div class="container">
        <h3>Step 3</h3>
        <p>Provide your address and some personal information. </p>
    </div>
</div>


<section class="contact-page"  ng-hide="loading">	
    <div class="container">					  
        <div class="row">					  			   
            <div class="col-md-9 col-sm-9 col-xs-12">
                <!--<button ng-click="previousStep(2)">Back</button>-->
                <form class="form-horizontal common-form wow fadeIn" id="ccfunnel-form-step1" method="post" name="form" ng-submit="submitForm(form.$valid)" novalidate>
                    <div class="row">
                        <!--<div class="form-group" >
                            <label for="address">Search your address:</label>
                            <input type="text" class="form-control" name="searchAddress" ng-model="searchAddress" onFocus="geolocate()" id="autocomplete">
                            <input type="text" class="form-control" name="searchAddress" ng-model="searchAddress"  id="autocomplete">
                        </div>-->
                        <!--<div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.street.$error.required]" >
                            <label for="street_number">Street:</label>
                            <input type="text" class="form-control" value="" name="street" ng-model="formData.street" id="street_number" required="">
                            <span ng-if="form.$submitted && form.street.$error.required" class="text text-danger">
                                The street field is required.
                            </span>
                        </div>-->
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.address.$error.required]" >
                            <label for="route">Address:</label>
                            <input type="text" class="form-control" value="" name="address" ng-model="formData.address" id="route" required="">
                            <span ng-show="form.$submitted && form.address.$error.required" class="text text-danger">The address field is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.city.$error.required]" >
                            <label for="route">City:</label>
                            <input type="text" class="form-control" value="" name="city" ng-model="formData.city" id="locality" required="">
                            <span ng-show="form.$submitted && form.city.$error.required" class="text text-danger">The city field is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.state.$invalid]" >
                            <label for="administrative_area_level_1">State:</label>
                            <!-- <input type="text" class="form-control" value="" name="state" ng-model="formData.state" id="administrative_area_level_1" required=""> -->
                            <?php include('states.php'); ?>
                            <span ng-show="form.$submitted && form.state.$error.required" class="text text-danger">The state field is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.zip.$error.required]" >
                            <label for="postal_code">Zip Code:</label>
                            <input type="text" class="form-control" value="" name="zip" ng-model="formData.zip" id="postal_code" required="">
                            <span ng-show="form.$submitted && form.zip.$error.required" class="text text-danger">The postal code field is required.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.ssn.$invalid]">
                            <label for="ssn">SSN: (<span ng-if="!completeSSN">Last 4</span><span ng-if="completeSSN">9</span> digits)</label>
                            <input type="text" ng-if="!completeSSN" class="form-control" name="ssn" minlength="4" ng-model="formData.ssn" id="ssn" required autocomplete="off">
                            <input type="text" ng-if="completeSSN" class="form-control" id="full-ssn" name="ssn" minlength="9" ng-model="formData.ssn" id="ssncomplete" required autocomplete="off">
                            <span ng-show="form.$submitted && form.ssn.$error.required" class="text text-danger">The SSN field is required.</span>
                            <span ng-show="form.$submitted && form.ssn.$error.minlength" class="text text-danger">The SSN you provided is not valid.</span>
                        </div>
                        <div class="form-group" ng-class="{true: 'has - error'}[form.$submitted && form.dob.$invalid]">
                            <label for="dob">DOB: (MM/DD/YYYY)</label>
                            <input type="text" class="form-control datepicker" name="dob" ng-model="formData.dob" required autocomplete="off">
                            <span ng-show="form.$submitted && form.dob.$error.required" class="text text-danger">The Date of birth field is required.</span>
                        </div>  <!-- ng-disabled="form.$invalid" -->
                        <div class="form-group">
                            <button type="button" class="btn btn-primary-outline pull-left btn-lg" ng-click="previousStep(2)">Previous</button>
                            <button type="submit" ng-disabled="form.$invalid" ng-class="form.$invalid?'btn-default':'btn-primary'" class="btn pull-right btn-lg">Next</button>               
                        </div>
                        <!-- <div class="text-center step-processing" ng-if="loading" ng-clock>
                            <i class="fa fa-spinner fa-spin" style="font-size:44px"></i>
                        </div> -->
                    </div>
                </form>	
            </div>	

            <div class="col-md-3 col-sm-3 col-xs-124">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                  Edit Personal Info
                </button>
            </div>

        </div>
    </div>
</section>
</div>
<script type="text/javascript">
    $(function () {
        $("#phone").mask("999-999-9999", {placeholder: "___-___-____"});
        $("#ssn").mask("0000", {placeholder: "____", clearEmpty: true});
        $("#ssncomplete").mask("000000000", {placeholder: "_________"});
        $("#administrative_area_level_1").mask("AA", {placeholder: "__"});
        $('.datepicker').datepicker({format: 'mm/dd/yyyy', autoclose: true});

    });

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')), {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();


        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
                $("#" + addressType).trigger('input');
            }
        }
    }
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    /*var scope = angular.element($("#body")).scope()
    scope.loading = false;*/
</script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwBitnf1DWi2H1j-MD5adcDGofK_49Bmc&libraries=places&callback=initAutocomplete&components=country:us" async defer></script>-->
