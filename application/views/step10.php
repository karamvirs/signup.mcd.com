<section class="hero-section">

    <!-- CONTAINER -->
    <div class="container">


        <div class="intro-section  text-center">
            <h1 class="intro">Reference site about Lorem Ipsum</h1>
			<p>Reference site about Lorem Ipsum, giving information on its origins.</p>
			</div><!----intro-section-->
			 <div class="clearfix"></div>
			 
			 <div class="row">
			 
			 <div class="col-md-7 padd_left_0 text-center"><img alt="" class="wow bounceInLefts" src="<?= base_url() . 'assets/images/Laptop.png' ?>"></div>	 
			 
			 <div class="col-md-5"><br>			 
			 <form class="form-horizontal login-form wow bounceInRight calltoaction">			 
			    <div class="row">
				<div class="col-md-6">
				<div class="form-group mar_right_0">
				<i class="ion-person"></i>
				<input type="text" placeholder="First Name" class="form-control">
				</div>	
				</div>
               <div class="col-md-6">
               <div class="form-group">
				<i class="ion-person"></i>
				<input type="text" placeholder="Last Name" class="form-control">
				</div>
				</div>
				
				
				</div>
				
				<div class="form-group">
				<i class="ion-ios-email"></i>
				<input type="text" placeholder="Email" class="form-control">
				</div>
				<div class="form-group">
				<i class="ion-ios-telephone-outline"></i>
				<input type="text" placeholder="Phone Number" class="form-control">
				</div>
				<div class="form-group">
				<button class="btn btn-orange btn-lg btn-block">Get Started - Now</button>
				</div>
				</form>	
				</div>	<!--- col-md-5-->		
			
			
			</div><!---row-->
			
</div><!----container-->
</section>


 <div class="clearfix"></div>




<section class="we-work">
    <!-- CONTAINER -->
   <div class="container">
   <div class="row">
    <div class="col-md-5  text-center wow bounceInLeft"><img alt="" src="<?= base_url() . 'assets/images/dummyscore.png' ?>"></div>
    <div class="col-md-7  padd_right_0 wow fadeIn">
	<div class="page-title">
	<h1>I am heading block.</h1>
	 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
	 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when 
	 an unknown printer took a galley of type and scrambled it to make a type specimen book. 
	 It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
</p>
	</div>
	
<div class="clearfix"></div>
	 <a class="btn btn-orange btn-lg" href="">Get Started Online</a>
	</div>

</div><!-- row -->
</div><!-- CONTAINER -->
</section>

<!--- -------------we-work END--------------- -->
 <div class="clearfix"></div>
 
 <section class="why-repair text-center">
    <div class="container ">
	<div class="page-title text-center white">		 
            <h1>Add "why" wording to this section</h1>
			</div>
			    <div class="clearfix"></div>
        <div class="row">     

            <div class="col-md-4 col-sm-6">
                <div class="box wow flipInX ">
				
                    <img src="<?= base_url() . 'assets/images/credit-score.png' ?>">
                    <h4>Loream Dollar Sit </h4>
                    <p>Reference site about Lorem Ipsum, giving information on its
origins, as well as a random Lipsum generator.</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="box wow flipInX ">
				  <img src="<?= base_url() . 'assets/images/online-portal.png' ?>">                  
                    <h4>Loream Dollar Sit</h4>
                    <p>Reference site about Lorem Ipsum, giving information on its
origins, as well as a random Lipsum generator.</p>
                </div>
            </div>


            <div class="col-md-4 col-sm-6">
                <div class="box wow flipInX ">
				<img src="<?= base_url() . 'assets/images/gaurantee.png' ?>">  
                    <h4>Loream Dollar Sit</h4>
                    <p>Reference site about Lorem Ipsum, giving information on its
origins, as well as a random Lipsum generator.</p>
                </div>
            </div>

			
			    
			  
			
			 
			
			
        </div>
    </div>
</section>


<section class="">
    <!-- CONTAINER -->
   <div class="container">
   <div class="row">
    <div class="col-md-5  text-center wow bounceInLeft"><img alt="" src="<?= base_url() . 'assets/images/lyft-man.png' ?>"></div>
    <div class="col-md-7  padd_right_0 wow fadeIn">
	<div class="page-title"><h1>Add features to this section</h1>
	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
	 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when 
	 an unknown printer took a galley of type and scrambled it to make a type specimen book. 
	 It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
</p></div>
	 <ul class="features-list large">
			 <li>Free Credit Score Monthly</li>
			 <li>No Credit Card Required - Ever!</li>
			 <li>Personalized Savings Advice</li>
			 <li>Daily Credit Monitoring</li>			 
			 </ul>
	
<div class="clearfix"></div><br>
	 <a class="btn btn-primary btn-lg wow bounceInBottom" href="">Signup Now</a>
	</div>

</div><!-- row -->
</div><!-- CONTAINER -->
</section>