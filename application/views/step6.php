<div class="page_title about-title">
    <div class="container">
        <h3>Step 6</h3>
        <p>Here are some recommendations to help improve your credit score.  The recommendations are based on your current credit profile and some of the answers to the questions you answered earlier.</p>
    </div>
</div>

<section class="pricing-plan-hero recommendations-row" ng-controller="ctrlStep6">
    <!-- CONTAINER -->
    <div class="container">

        <div class="clearfix"></div>
        <div class="pricing-boxes"> 
            <div class="col-md-10 col-md-offset-1 padd_0">
                <!-- Mobile Nav tabs -->
                <!-- tab-content -->
                <div class="tab-content">
                    <p></p>
                    <div class="col-md-12  col-sm-12 wow fadeIn" >
                        <div class="pricing-table">
                            <div class="pricing-content">
                                <p ng-if="!processing" ng-repeat="(scoreFactorKey, scoreFactor) in recommendations">
                                    {{scoreFactor}}
                                </p>
                                <p ng-if="!processing" class="text-center"><a ng-click="goto()" class="btn btn-sm btn-primary">Login into my dashboard</a></p>
                                <p ng-if="processing" class="text-center"><i class="fa fa-spin fa-spinner"></i></p>
                            </div> 
                        </div>
                    </div>
                </div>
                <!-- tab-content -->
                <div class="clearfix"></div> 
            </div>
        </div>
        <div class="clearfix"></div>
    </div><!----container-->
</section>