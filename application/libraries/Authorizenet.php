<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH.'vendor/autoload.php';
//require_once APPPATH.'/third_party/autherizenet/vendor/constants.php';

use net\authorize\api\constants\ANetEnvironment;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class Authorizenet {

    /**
     *
     * @var $auth - authorize merchant
     */
    private $auth;

    /**
     *
     * @var $testMode - Authorise test mode flag
     * @return boolean
     */
    private $testMode;

    /**
     *
     * @var $authUrl - auth url
     * @return authentication url
     */
    private $authUrl;

    /**
     *
     * @var $authLoginId - auth login ID
     */
    private $authLoginId;

    /**
     *
     * @var $authTransactionId - auth transaction ID
     */
    private $authTransactionId;

    /**
     *
     * @var $merchantAuth - Merchant auth type instance
     */
    private $merchantAuth;

    /**
     *
     * @var $cardNumber - credit card number
     */
    private $cardNumber;

    /**
     *
     * @var $expiryYear - credit card expiry year
     */
    private $expiryYear;

    /**
     *
     * @var $expiryMonth - credit card expiry month
     */
    private $expiryMonth;

    /**
     *
     * @var $cvv - credit card cvv
     */
    private $cvv;

    /**
     *
     * @var $creditCard - credit card instance
     */
    private $card;

    /**
     *
     * @var $paymentType
     */
    private $paymentType;

    /**
     *
     * @var $billingAddress
     */
    private $billingAddress;

    /**
     *
     * @var $customer
     */
    private $customer;

    /**
     *
     * @var $order
     */
    private $order;

    /**
     *
     * @var $subscription
     */
    private $subscription;

    /**
     *
     * @var $refId
     */
    private $refId;

    /**
     * @var $transaction
     */
    private $transaction;

    /**
     * @var $responseData
     * @type array
     */
    private $responseData = [];

    /**
     *
     * @var $isRecurring
     *
     */
    private $isRecurring = false;
    /**
     *
     * @var $isCard
     *
     */
    private $isCard;

    public function __construct()
    {
        $CI = & get_instance();

        $this->testMode = $CI->config->item('AUTHNET_MODE') == "SANDBOX";

        $this->authLoginId = $CI->config->item('AUTHNET_LOGIN_ID');
        $this->authTransactionId = $CI->config->item('AUTHNET_TRANSACTION_ID');

        if ($this->testMode == true) {
            $this->authUrl = ANetEnvironment::SANDBOX;
        } else {
            $this->authUrl = ANetEnvironment::PRODUCTION;
        }

        $merchantAuthType = new AnetAPI\MerchantAuthenticationType;
        $merchantAuthType->setName($this->authLoginId);
        $merchantAuthType->setTransactionKey($this->authTransactionId);
        $this->merchantAuth = $merchantAuthType;

        $this->refId = 'ref' . time();
        
    }

    /**
     * @param array
     * @return $this
     */
    public function initialize($description = '', $isCard = true)
    {

        $this->order = new AnetAPI\OrderType();
        $this->order->setInvoiceNumber(uniqid('INV-'));

        if ($description) {
            $this->order->setDescription($description);
        }

        //$this->isRecurring = $isRecurring;

        $this->isCard = $isCard;

        return $this;
    }

    /**
     * @param $cardNumber, $expiryMonth, $expiryYear, $cvv
     * @return $this
     */
    public function creditCard($cardNumber, $expiryMonth, $expiryYear, $cvv)
    {
        try {
            $this->cardNumber = $cardNumber;
            $this->expiryMonth = $expiryMonth;
            $this->expiryYear = $expiryYear;
            $this->cvv = $cvv;

            $this->card = new AnetAPI\CreditCardType();
            $this->card->setCardNumber($this->cardNumber);
            $this->card->setExpirationDate($this->expiryYear . "-" . $this->expiryMonth);
            $this->card->setCardCode($this->cvv);

            $this->paymentType = new AnetAPI\PaymentType();
            $this->paymentType->setCreditCard($this->card);

            return $this;
        } catch (Exception $e) {
            throw new Exception('Insufficient card information provided.', 105);
        }
    }

    /**
     * @param array
     * return $this
     */
    public function transaction($config, $amount)
    {
        try {
            $this->billingAddress = new AnetAPI\CustomerAddressType();
            $this->billingAddress->setFirstName($config['firstname']);
            $this->billingAddress->setLastName($config['lastname']);
            $this->billingAddress->setAddress($config['address']);
            $this->billingAddress->setCity($config['city']);
            $this->billingAddress->setState($config['state']);
            $this->billingAddress->setZip($config['zip']);
            $this->billingAddress->setCountry("USA");

            $this->customer = new AnetAPI\CustomerDataType();
            $this->customer->setType('individual');

            if (isset($config['email'])) {
                $this->customer->setEmail($config['email']);
            }

            $transactionRequestType = new AnetAPI\TransactionRequestType();
            $transactionRequestType->setTransactionType("authCaptureTransaction");
            $transactionRequestType->setAmount($amount);
            $transactionRequestType->setOrder($this->order);

            $transactionRequestType->setPayment($this->paymentType);
            $transactionRequestType->setBillTo($this->billingAddress);
            $transactionRequestType->setCustomer($this->customer);

            $request = new AnetAPI\CreateTransactionRequest();
            $request->setMerchantAuthentication($this->merchantAuth);
            $request->setRefId($this->refId);
            $request->setTransactionRequest($transactionRequestType);

            $controller = new AnetController\CreateTransactionController($request);
            $this->transaction = $controller->executeWithApiResponse($this->authUrl);
            return $this;
        } catch (Exception $e) {
            throw new Exception($e->getCode(), $e->getMessage());
        }
    }

    /**
     * @param $product instance, $data
     * @return $this
     */
    public function recurring($product, $data, $isMonthly = true)
    {

        if (empty($product) || !is_object($product)) {
            throw new Exception('Product object expected, NULL given.', 104);
        }

        if (empty($data)) {
            throw new Exception('Insufficient user information', 106);
        }

        $subscription = new AnetAPI\ARBSubscriptionType();
        // $subscription->setName("MCD Subscription: " . $product->name);//commented this as the name cannot be longer than 50 characters
        $subscription->setName($product->name);
        $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
     
        $interval->setLength($isMonthly?30:365);
        
        $interval->setUnit("days");
        $paymentSchedule = new AnetAPI\PaymentScheduleType();
        $paymentSchedule->setInterval($interval);
        $paymentSchedule->setStartDate(new \DateTime());
        $paymentSchedule->setTotalOccurrences("9999");
        $paymentSchedule->setTrialOccurrences("0");
        $subscription->setPaymentSchedule($paymentSchedule);
        $subscription->setAmount($product->price);
        $subscription->setTrialAmount("0.00");

        if ($this->isCard) {
            $subscription->setPayment($this->paymentType);
        }


        $subscription->setOrder($this->order);

        $billTo = new AnetAPI\NameAndAddressType();
        $billTo->setFirstName($data['firstname']);
        $billTo->setLastName($data['lastname']);
        $subscription->setBillTo($billTo);

        $request = new AnetAPI\ARBCreateSubscriptionRequest();
        $request->setmerchantAuthentication($this->merchantAuth);
        $request->setRefId($this->refId);
        $request->setSubscription($subscription);
        $controller = new AnetController\ARBCreateSubscriptionController($request);
        $this->transaction = $controller->executeWithApiResponse($this->authUrl);
        return $this;
    }

/**
 * @param $profileid, $paymentprofileid, $amount
 */
    public function chargeCustomerProfile($profileid, $paymentprofileid, $amount)
    {
        try {
            $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
            $profileToCharge->setCustomerProfileId($profileid);
            $paymentProfile = new AnetAPI\PaymentProfileType();
            $paymentProfile->setPaymentProfileId($paymentprofileid);
            $profileToCharge->setPaymentProfile($paymentProfile);
            $transactionRequestType = new AnetAPI\TransactionRequestType();
            $transactionRequestType->setTransactionType("authCaptureTransaction");
            $transactionRequestType->setAmount($amount);
            $transactionRequestType->setProfile($profileToCharge);
            $request = new AnetAPI\CreateTransactionRequest();
            $request->setMerchantAuthentication($this->merchantAuth);

            $request->setTransactionRequest($transactionRequestType);
            $controller = new AnetController\CreateTransactionController($request);
            $this->transaction = $controller->executeWithApiResponse($this->authUrl);
            return $this;
        } catch (Exception $e) {
            throw new Exception($e->getCode(), $e->getMessage());
        }
    }

    /**
     *
     * @param $product, $data
     * @return $this
     */
    public function updateSubscription($product, $data)
    {
        try { 
            $subscription = new AnetAPI\ARBSubscriptionType();
//set profile information
            $profile = new AnetAPI\CustomerProfileIdType();
            $profile->setCustomerProfileId($data['profile']);
            $profile->setCustomerPaymentProfileId($data['payment_profile']);
            $profile->setCustomerAddressId($data['customer_address']);

            $subscription->setAmount($product->price);

            if ($this->isCard) {
                $subscription->setPayment($this->paymentType);
            }

            $request = new AnetAPI\ARBUpdateSubscriptionRequest();
            $request->setMerchantAuthentication($this->merchantAuth);
            $request->setRefId($this->refId);
            $request->setSubscriptionId($data['subscription']);
            $request->setSubscription($subscription);
            $controller = new AnetController\ARBUpdateSubscriptionController($request);
            $this->transaction = $controller->executeWithApiResponse($this->authUrl);
            return $this;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     *
     * @param $product, $data
     * @return $this
     */
    public function cancelSubscription($subscriptionId)
    {

        try {
            $request = new AnetAPI\ARBCancelSubscriptionRequest();
            $request->setMerchantAuthentication($this->merchantAuth);
            $request->setRefId($this->refId);
            $request->setSubscriptionId($subscriptionId);
            $controller = new AnetController\ARBCancelSubscriptionController($request);
            $this->transaction = $controller->executeWithApiResponse($this->authUrl);
            return $this;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     *
     * @param $origin
     * @return response array
     * @throws Exception
     */
    public function cancelResponse($origin = true)
    {
        if (!$this->transaction || $this->transaction == null) {
            throw new Exception('Payment could not be processed at the moment, please try later');
        }

        if ($origin == true) {
            return $this->transaction;
        }

        try {
            $transactionCode = $this->transaction->getMessages()->getResultCode();

            $this->responseData['data']['ref_id'] = $this->transaction->getRefId();
            $this->responseData['data']['transaction_status'] = $transactionCode;

            if ($transactionCode == 'Ok'):
                $this->responseData['status'] = 'success';
                $this->responseData['message'] = 'Subscription canceled';
                return $this->responseData;
            endif;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
/**
 * @param boolean
 * @return $this
 */
    public function updateCustomerPaymentProfile($customerProfileId = null, $customerPaymentProfileId = null,$user = false)
    {

        $request = new AnetAPI\UpdateCustomerPaymentProfileRequest();
        $request->setMerchantAuthentication($this->merchantAuth);
        $request->setCustomerProfileId($customerProfileId);
        $controller = new AnetController\GetCustomerProfileController($request);

        // Create the Customer Payment Profile object
        $paymentprofile = new AnetAPI\CustomerPaymentProfileExType();
        $paymentprofile->setCustomerPaymentProfileId($customerPaymentProfileId);

        if($user):
            $billto = new AnetAPI\CustomerAddressType();
            $billto->setFirstName($user->firstname);
            $billto->setLastName($user->lastname);
            $paymentprofile->setBillTo($billto);
        endif;

        if ($this->isCard) {
            $paymentprofile->setPayment($this->paymentType);
        }
        


        // Submit a UpdatePaymentProfileRequest
        $request->setPaymentProfile($paymentprofile);
        $controller = new AnetController\UpdateCustomerPaymentProfileController($request);
        $response = $controller->executeWithApiResponse($this->authUrl);
        $this->transaction = $response;

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            return $this;
        }
        
        //throw exception only in case when updating card, otherwise dont
        if(!$user)
            throw new Exception("We are unable to update your card information at the moment, please try later.");

    }

/**
 * @params customerProfileId, customerPaymentProfileId
 */
    public function getCustomerPaymentProfile($customerProfileId = "36731856", $customerPaymentProfileId = "33211899")
    {
        try {
            //request requires customerProfileId and customerPaymentProfileId
            $request = new AnetAPI\GetCustomerPaymentProfileRequest();
            $request->setMerchantAuthentication($this->merchantAuth);
            $request->setCustomerProfileId($customerProfileId);
            $request->setCustomerPaymentProfileId($customerPaymentProfileId);
            $controller = new AnetController\GetCustomerPaymentProfileController($request);
            $this->transaction = $controller->executeWithApiResponse($this->authUrl);
            return $this;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
/**
 * @param boolean
 * @return subscription response
 */
    public function paymentProfileResponse($origin = true)
    {

        if (!$this->transaction || $this->transaction == null) {
            throw new Exception('Payment profile not available');
        }

        if ($origin == true) {
            return $this->transaction;
        }

        try {
            $transactionResponse = $this->transaction->getPaymentProfile();
            $transactionCode = $this->transaction->getMessages()->getResultCode();

            if ($transactionResponse && $transactionCode == 'Ok'):
                $card = $transactionResponse->getPayment()->getCreditCard();
                $this->responseData['data'] = [
                    'card' => $card->getCardNumber(),
                    'type' => $card->getCardType(),
                ];
                $this->responseData['status'] = 'success';
                $this->responseData['message'] = 'payment profile found.';
            else:
                $this->responseData['status'] = 'error';
                $this->responseData['message'] = 'Can not get payment profile.';
            endif;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());

        }
        return $this->responseData;
    }

    /**
     * @param boolean
     * @return subscription response
     */
    public function subscriptionResponse($origin = true, $update = false)
    {

        if (!$this->transaction || $this->transaction == null) {
            throw new Exception('Payment could not be processed at the moment, please try later');
        }

        if ($origin == true) {
            return $this->transaction;
        }

        $transactionResponse = $this->transaction->getProfile();

        $transactionCode = $this->transaction->getMessages()->getResultCode();

        if ($transactionResponse):
            $this->responseData['data'] = [
                'profile_id' => $transactionResponse->getCustomerProfileId(),
                'payment_profile_id' => $transactionResponse->getCustomerPaymentProfileId(),
                'customer_address_id' => $transactionResponse->getCustomerAddressId(),
                'transaction_status' => $transactionCode,
                'is_subscription' => 1,
            ];
            if ($update == false) {
                $this->responseData['data']['subscription_id'] = $this->transaction->getSubscriptionId();
            }

        endif;

        $this->responseData['data']['ref_id'] = $this->transaction->getRefId();

        if ($transactionCode == 'Ok'):
            $this->responseData['status'] = 'success';
            $this->responseData['message'] = $update ? 'Subscription updated' : 'Subscription has been created';
            return $this->responseData;
        endif;

        $this->responseData['status'] = 'error';
        if ($transactionResponse):
            $this->responseData['message'] = '<p>' . implode('</p><p>', $transactionResponse->getErrors()) . '</p>';
            return $this->responseData;
        endif;

        $error = $this->transaction->getMessages()->getMessage()[0];

        switch ($error->getCode()):
    case 'E00003':
        throw new Exception($error->getCode().':Incorrect card information, please check and try again.');
        break;
    default:
        throw new Exception($error->getCode().':Payment Failed: ' . $error->getText());
        break;
        endswitch;

        return $this->responseData;
    }

    /**
     * @param boolean
     * @return transaction response
     */
    public function transactionResponse($origin = true)
    {

            if (!$this->transaction || $this->transaction == null) {
                throw new Exception('Payment could not be processed at the moment, please try later');
            }

            if ($origin == true) {
                return $this->transaction;
            }

            $transactionResponse = $this->transaction->getTransactionResponse();
            $transactionCode = $this->transaction->getMessages()->getResultCode();
            if ($transactionResponse):
                $this->responseData['data'] = [
                    'trans_id' => $transactionResponse->getTransId(),
                    'trans_hash' => $transactionResponse->getTransHash(),
                    'account_no' => $transactionResponse->getAccountNumber(),
                    'account_type' => $transactionResponse->getAccountType(),
                    'message' => $transactionResponse->getMessages()[0]->getDescription(),
                    'auth_code' => $transactionResponse->getAuthCode(),
                    'avs_result_code' => $transactionResponse->getAvsResultCode(),
                    'cvv_result_code' => $transactionResponse->getCvvResultCode(),
                    'cavv_result_code' => $transactionResponse->getCavvResultCode(),
                    'ref_trans_id' => $transactionResponse->getRefTransID(),
                    'transaction_status' => $transactionCode,
                ];
            endif;

            $this->responseData['data']['ref_id'] = $this->transaction->getRefId();

            if ($transactionCode == 'Ok'):
                $this->responseData['status'] = 'success';
                $this->responseData['message'] = 'This transaction has been approved.';
                return $this->responseData;
            endif;

            $this->responseData['status'] = 'error';
            if ($transactionResponse):
                $this->responseData['message'] = '<p>' . implode('</p><p>', $transactionResponse->getErrors()) . '</p>';
                return $this->responseData;
            endif;

            $error = $this->transaction->getMessages()->getMessage()[0];

            switch ($error->getCode()):
        case 'E00003':
            throw new Exception('Incorrect card information, please check and try again.');
            break;
        default:
            throw new Exception('Payment Failed: ' . $error->getText());
            break;
            endswitch;

            return $this->responseData;
    }

    /**
     * Create customer profile from transaction
     */

    public function createCustomerProfileFromTransaction($transId, $user)
    {
            try {

                $customerProfile = new AnetAPI\CustomerProfileBaseType();
                $customerProfile->setMerchantCustomerId($user->id);
                $customerProfile->setEmail($user->email);
                $customerProfile->setDescription($user->fullName() . '\'s Profile');

                $request = new AnetAPI\CreateCustomerProfileFromTransactionRequest();
                $request->setMerchantAuthentication($this->merchantAuth);
                $request->setTransId($transId);
                // You can either specify the customer information in form of customerProfileBaseType object
                $request->setCustomer($customerProfile);


                // You can just provide the customer Profile ID
                //$request->setCustomerProfileId("123343");
                $controller = new AnetController\CreateCustomerProfileFromTransactionController($request);
                $response = $controller->executeWithApiResponse($this->authUrl);
                
                if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                    $this->updateCustomerPaymentProfile($response->getCustomerProfileId(),$response->getCustomerPaymentProfileIdList()[0],$user);
                    
                    $return = [
                        'status' => 'success',
                        'data' => [
                            'profile_id' => $response->getCustomerProfileId(),
                            'payment_profile_id' => $response->getCustomerPaymentProfileIdList()[0]
                        ]
                    ];
                        
                    
                    $return['payment_profile_updated'] = $this->transaction->getMessages()->getResultCode() == "Ok";
                        
                    return $return;
                } else {
                    return ['status' => 'error'];
                }
                return $response;
            } catch (Exception $e) {
                return ['status' => 'error','data'=>$e->getMessage()];
            }
    }

    public function createCustomerPaymentProfile($user)
    {
        $existingcustomerprofileid = $user->profile_id;
        try {
            $billto = new AnetAPI\CustomerAddressType();
            $billto->setFirstName($user->firstname);
            $billto->setLastName($user->lastname);
            $billto->setAddress($user->address);
            $billto->setCity($user->city);
            $billto->setState($user->state);
            $billto->setZip($user->zip);
            $billto->setCountry("USA");
            $billto->setPhoneNumber($user->phone);
            // Create a new Customer Payment Profile object
            $paymentprofile = new AnetAPI\CustomerPaymentProfileType();
            $paymentprofile->setCustomerType('individual');
            $paymentprofile->setBillTo($billto);
            $paymentprofile->setDefaultPaymentProfile(true);
            $paymentprofiles[] = $paymentprofile;
            // Assemble the complete transaction request
            $paymentprofilerequest = new AnetAPI\CreateCustomerPaymentProfileRequest();
            $paymentprofilerequest->setMerchantAuthentication($this->merchantAuth);
            // Add an existing profile id to the request
            $paymentprofilerequest->setCustomerProfileId($existingcustomerprofileid);
            $paymentprofilerequest->setPaymentProfile($paymentprofile);
            $paymentprofilerequest->setValidationMode("liveMode");
            // Create the controller and get the response
            $controller = new AnetController\CreateCustomerPaymentProfileController($paymentprofilerequest);
            $response = $controller->executeWithApiResponse($this->authUrl);
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                return ['status' => 'success','data' => ['payment_profile_id' => $response->getCustomerPaymentProfileId()]];
            } else {
                return ['status' => 'error'];
            }
            return $response;
        } catch (Exception $e) {
            return ['status' => 'error'];
        }
    }

    function createSubscriptionFromCustomerProfile($customer, $product, $intervalLength = 30, $customerAddressId = null
) {

    $customerProfileId = $customer->profile_id;
    $customerPaymentProfileId = $customer->payment_profile_id;
    
    $subscription = new AnetAPI\ARBSubscriptionType();
    $subscription->setName("Sample Subscription");
    $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
    $interval->setLength($intervalLength);
    $interval->setUnit("days");
    $paymentSchedule = new AnetAPI\PaymentScheduleType();
    $paymentSchedule->setInterval($interval);
    $paymentSchedule->setStartDate(new \DateTime());
    $paymentSchedule->setTotalOccurrences("12");
    $paymentSchedule->setTrialOccurrences("1");
    $subscription->setPaymentSchedule($paymentSchedule);
    $subscription->setAmount($product->price);
    $subscription->setTrialAmount("0.00");

    $profile = new AnetAPI\CustomerProfileIdType();
    $profile->setCustomerProfileId($customerProfileId);
    $profile->setCustomerPaymentProfileId($customerPaymentProfileId);
    if($customerAddressId)
        $profile->setCustomerAddressId($customerAddressId);
    /* else{
        $billTo = new AnetAPI\NameAndAddressType();
        $billTo->setFirstName($customer->firstname);
        $billTo->setLastName($customer->lastname);
        $subscription->setBillTo($billTo);
    } */


    $subscription->setProfile($profile);
    $request = new AnetAPI\ARBCreateSubscriptionRequest();
    $request->setmerchantAuthentication($this->merchantAuth);
    $request->setRefId($this->refId);
    $request->setSubscription($subscription);
    $controller = new AnetController\ARBCreateSubscriptionController($request);
    $response = $controller->executeWithApiResponse($this->authUrl);
    
    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {

        return [
            'status' => 'success',
            'data' => [
                'subscription_id' => $response->getSubscriptionId(),
                'profile_id' => $response->getProfile()->getCustomerProfileId(),
                'payment_profile_id' => $response->getProfile()->getCustomerPaymentProfileId(),
                'ref_id' => $response->getRefId(),
                'transaction_status' => $response->getMessages()->getResultCode()
            ]
        ];
    } else {
        return [
            'status' => 'error',
            'error' => $errorMessages = $response->getMessages()->getMessage()[0]->getText()
        ];
    }
}

}
