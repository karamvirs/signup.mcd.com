<?php

require_once APPPATH . 'third_party/stripe/vendor/autoload.php';

class Payment {

    private $secretKey = null;
    private $tokenKey = null;
    private $response = array();

    public function __construct($param) {
        $this->secretKey = $param['secretKey'];
        $this->tokenKey = $param['token'];
        \Stripe\Stripe::setApiKey($this->secretKey);
    }

    public function charge($data) {

        try {
            $charge = \Stripe\Charge::create(array(
                        "amount" => $data['price'],
                        "currency" => "usd",
                        "description" => $data['plan'],
                        "source" => $this->tokenKey,
            ));
            $response['status'] = true;
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $response['status'] = false;
            $response['message'] = $body['error'];
        } catch (\Stripe\Error\RateLimit $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
            // Too many requests made to the API too quickly
        } catch (\Stripe\Error\InvalidRequest $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
            // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Error\Authentication $e) {
            $body = $e->getJsonBody();
            $response['status'] = false;
            $response['message'] = $body['error'];
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        if (isset($response['status']) && $response['status'] == true) {
            $response['data'] = $charge->__toArray(TRUE);
        }
        return $response;
    }

}
