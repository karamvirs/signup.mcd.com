function wakeupLib() {
    $('.datepicker').datepicker({format: 'mm-dd-yyyy', autoclose: true});
}
completeSNN = false;
var app = angular.module('stepA', ["ngRoute","ui"]);
app.config(function ($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: mainPage + "/step/1",
        controller: 'page_load'
    }).when(mainPage + "/step/:step_id", {
        templateUrl: function (params) {
            return mainPage + '/step/' + params.step_id;
        }
    }).when("/step/1/signup", {
        templateUrl: mainPage + "/step/1/signup"
    })
});
app.run(function ($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        window.scrollTo(0, 0);
        if (typeof (current) == 'undefined')
            return false;
        if (current.loadedTemplateUrl == mainPage + '/step/1')
            $('.navbar-static-top').removeClass('step-page');
        else
            $('.navbar-static-top').addClass('step-page');
    });
});
app.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.directive('percentNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                } else {
                    clean = decimalCheck[0];
                }
//                if (val !== clean) {
                if (val) {
                    clean = (val != '%') ? clean : 0;
                    clean = (clean > 100) ? '' : clean;

                    ngModelCtrl.$setViewValue(clean + '%');
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.controller('page_load', function ($scope, $interval, $location, $http, $window) {
    
    $scope.loading = true;
    $scope.logggedIn = false;
    $scope.page = {};
    $scope.page.alert = '';
    $scope.page.flash = '';
    $scope.userData = {};
    $scope.products = [];
    $scope.productAlias = '';
    $scope.processing = true;
    
    
    $scope.paymentDone = paymentDone;

    $http({
        method: 'POST',
        url: apiUrl + 'products',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param({token: token}),
    }).then(function success(r) {
        $scope.products = r.data.products;
    });

    $interval(function () {
        $('#message').fadeOut('slow');
        $scope.page.alert = '';
    }, 120000);

    $http({
        method: 'POST',
        url: site_url + 'app/usersession',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param({'type': 'retrieve'})
    }).then(function (r) {
    
    
        if (r.data.status_code == 200) {
            customerId = r.data.userdata.customerId;
            $scope.userData.step1 = r.data.userdata.one;
            $scope.userData.step2 = r.data.userdata.two;
            $scope.userData.step3 = r.data.userdata.three;
            token = r.data.userdata.token;

            if(typeof(r.data.userdata.paymentDone) != 'undefined'){
                $scope.paymentDone = r.data.userdata.paymentDone;
            }

            if (customerId != "") {
                $scope.logggedIn = true;
                $scope.formData = {token: token, uuid: customerId};

                $http({
                    method: 'POST',
                    url: apiUrl + 'getinfo',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.formData)
                }).then(function success(response) {
                    
                    content = response.data;
                    
                    if (content.status == true) {
                        if (content.data.step) {
                            customerId = content.data.uuid;
                            $scope.userData.firstname = content.data.firstname;
                            $scope.stepId = content.data.step;
                            if(content.data.step == 6)
                                $scope.stepId = 5;
                            $location.path(mainPage + "/step/" + $scope.stepId);
                            // $scope.loading = false;
                            $window.scrollTo(0, 0);
                            $scope.page.alert = 'success';
                            $scope.page.flash = content.message;
                        }
                    }
                }, function error(response) {
                    $window.scrollTo(0, 0);
                    $scope.page.alert = 'info';
                    $scope.page.flash = "Something went wrong. Please try again";
                });
            }
            
        }else{
            
            $scope.formData = {token: token};

            var uid = getQueryString('id');

            token = token?token:getQueryString('t');
            // console.log(token);
            var firstname = getQueryString('firstname');
            var lastname = getQueryString('lastname');
            var email = decodeURIComponent(getQueryString('email'));
            var phone = getQueryString('phone');

            if(uid){
                customerId = uid;
                var d = {token:token,firstname:firstname,lastname:lastname,email:email,phone:phone,customerId:customerId};
                $scope.userData.step1 = d;
                $http({
                    method: 'POST',
                    url: site_url + 'app/usersession',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param(d)+'&customerId='+customerId+'&type=set&step=1'
                }).then(function (r) {
                    if (r.data.status_code == 199) {
                        $scope.page.alert = 'success';
                        $scope.page.flash = '';
                        $scope.stepId = '2';
                        $window.scrollTo(0, 0);
                        $location.path(mainPage + "/step/2");
                    }else{
                        $scope.page.alert = 'info';
                        $scope.page.flash = 'Something went wrong';
                        $scope.stepId = '1';
                        $window.scrollTo(0, 0);
                        $location.path(mainPage + "/step/1/signup");

                    }
                    // $scope.loading = false;
                });
            }else{
                $scope.loading = false;
                
            }

            /*if (typeof ($scope.userData.step1) != 'undefined') {
                $scope.formData = $scope.userData.step1;
            }*/
        }
        

        
    });
    $scope.logout = function (action) {
        $http({
            method: 'POST',
            url: site_url + 'app/usersession',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({'type': 'logout', 'customerId': customerId})
        }).then(function (r) {
            if (r.data.status_code == 201) {
                //customerId = '';
                $scope.logggedIn = false;
                //console.log('token'+ token);
                //redirect user to MCD dashboard
                window.top.location.href = apiUrl + "login/"+customerId+'/'+token;
                /*
                if (action != 'dashboard')
                    $location.path(mainPage + "/step/1");
                */
            }
        });
    };

 $scope.logout_no_redirect = function (action) {
        $http({
            method: 'POST',
            url: site_url + 'app/usersession',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({'type': 'logout', 'customerId': customerId})
        }).then(function (r) {
            if (r.data.status_code == 201) {
                //customerId = '';
                $scope.logggedIn = false;

                setTimeout(function(){ window.top.location.href = 'http://107.180.55.213/acr/signup/'; },10000);
                
            }
        });
    };

    $scope.checkUUID = function (pid) {
        if (customerId == "") {
            $scope.page.alert = 'info';
            $scope.page.flash = "UUID is missing";
            $("#container").remove();
            $window.scrollTo(0, 0);
            $location.path(mainPage + 'step/1');
            return false;
        }
    };
    $scope.previousStep = function (step) {
        // alert(mainPage + '/step/' + step);
        $scope.page = {};
        $location.path(mainPage + '/step/' + step);
    };

    
    $scope.pi_processing = false;
    $scope.pi_result = false;
    $scope.pi_status = false;

    $scope.submitPIForm = function (isValid) {
// console.log($scope.pi_status);
    $scope.formData = {token: token};
    $scope.formData = $scope.userData.step1;
   
    $scope.pi_processing = true;
        
        if (isValid == true) {
            // console.log($scope.formData);
            $scope.userData.step1 = $scope.formData;
            $http({
                method: 'POST',
                url: apiUrl + 'saveinfo/editpi',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.formData) + '&uuid='+customerId
            }).then(function success(response) {
                content = response.data;
                if (content.status == true) {
                    
                    customerId = content.data.uuid;
                    $http({
                        method: 'POST',
                        url: site_url + 'app/usersession',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.formData)+'&customerId='+customerId+'&type=set&step=1'
                    }).then(function (r) {
                        /*$scope.loading = false;
                        $scope.page.alert = 'success';
                        $scope.page.flash = content.message;
                        $scope.stepId = content.data.step;
                        $window.scrollTo(0, 0);*/
                        $scope.pi_status = true;
                        $scope.pi_result = content.message;
                        
                    });
                    
                } else {
                    /*$scope.loading = false;
                    $scope.page.alert = 'danger';*/

                    $scope.pi_result = content.message;
                }
                $scope.pi_processing = false;

            }, function error(response) {
                /*$window.scrollTo(0, 0);
                $scope.loading = false;
                $scope.page.alert = 'info';
                $scope.page.flash = "Something went wrong. Please try again";*/
                $scope.pi_result = "Something went wrong. Please try again";
                $scope.pi_processing = false;
//                
            });
        } else {
            /*$window.scrollTo(0, 0);
            $scope.loading = false;
            $scope.page.alert = 'info';
            $scope.page.flash = "Please fix errors before proceeding";*/

            $scope.pi_result = "Please fix errors before proceeding";
        }
    };



});

app.controller('editpi', function ($scope, $location, $http, $window) {

    });


app.controller('ctrlStep1', function ($scope, $location, $http, $window) {
    $scope.formData = {token: token,d:d};
    // console.log($scope.formData);   
    // $("#phone").mask("999-999-9999", {placeholder: "___-___-____"});

    var firstname = getQueryString('firstname');
    var lastname = getQueryString('lastname');
    var email = getQueryString('email');
    var phone = getQueryString('phone');

    if(typeof($scope.userData.step1) == 'undefined'){
        // alert('here');
        $scope.formData.firstname = firstname;
        $scope.formData.lastname = lastname;
        $scope.formData.email = decodeURIComponent(email);
        $scope.formData.phone = phone; 
        $('#phone').trigger('click');
        // $('#phone').blur();
    }
    

    $scope.submitForm = function (isValid) {
        
        $scope.loading = true;
        if (isValid == true) {
            // console.log($scope.formData);
            $scope.userData.step1 = $scope.formData;
            $http({
                method: 'POST',
                url: apiUrl + 'saveinfo',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.formData) + '&uuid='+customerId
            }).then(function success(response) {
                content = response.data;
                if (content.status == true) {
                    if (content.data.step) {
                        customerId = content.data.uuid;
                        $http({
                            method: 'POST',
                            url: site_url + 'app/usersession',
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: $.param($scope.formData)+'&customerId='+customerId+'&type=set&step=1'
                        }).then(function (r) {
                            $scope.loading = false;
                            $scope.page.alert = 'success';
                            $scope.page.flash = content.message;
                            $scope.stepId = content.data.step;
                            $window.scrollTo(0, 0);
                            $location.path(mainPage + "/step/" + content.data.step);
                        });
                    } else {
                        $scope.page.alert = 'success';
                        $scope.page.flash = content.message;
                        $window.scrollTo(0, 0);
                        $location.path(mainPage + "/step/2");
                    }
                } else {
                    $scope.loading = false;
                    $scope.page.alert = 'danger';
                    $scope.page.flash = content.message;
                }
            }, function error(response) {
                $window.scrollTo(0, 0);
                $scope.loading = false;
                $scope.page.alert = 'info';
                $scope.page.flash = "Something went wrong. Please try again";
//                
            });
        } else {
            $window.scrollTo(0, 0);
            $scope.loading = false;
            $scope.page.alert = 'info';
            $scope.page.flash = "Please fix errors before proceed";
        }
    };
});
app.controller('ctrlStep2', function ($scope, $http, $location, $window) {
    

    $scope.checkUUID(2);
    
    $scope.loading = false;

    // $scope.loading = false;
    
    // phone = getQueryString('phone');
    
    $scope.formData = {token: token, uuid: customerId,d:d,productAlias:product_alias};
    $scope.goals = ["Please make a selection", "Purchase a Home", "Refinance", "Improve Credit Worthiness", "Apply for Credit Cards", "Apply for Personal Loans", "I need a car","Other"];
    $scope.statuses = ["Please make a selection", "Employed", "Unemployed", "Retired", "Disabled", "Self-Employed", "Student", "Military","Other"];
    $scope.times = ["Please make a selection", "Less than 90 days", "3 - 6 Months", "6 - 12 Months", "1+ Year","Other"];
    $scope.formData.goal = "";
    $scope.formData.time = "";
    $scope.formData.status = "";
    $scope.formData.owner = "No";
    $scope.formData.loan_amount = "";
    $scope.formData.interest_rate = "";
    $scope.formData.refinaced = "No";
    $scope.formData.refinacne_date = "";
    /*console.log(phone);
    console.log($scope.goals);
    console.log($scope.statuses);
    console.log($scope.times);*/
    

    if (typeof ($scope.userData.step2) != 'undefined') {
        $scope.formData = $scope.userData.step2;
    }
    $scope.submitForm = function (isValid) {
        $window.scrollTo(0, 0);
        $scope.scrollup = true;
        // window.parent.parent.scrollTo(0,0);
        // scrltop();
        $scope.loading = true;
        // $scope.pl.loading = true;
        if (isValid == true) {
            $scope.userData.step2 = $scope.formData;
            $http({
                method: 'POST',
                url: apiUrl + 'saveinfo/2',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.formData)
            }).then(function success(response) {
                $scope.scrollup = false;
                $window.scrollTo(0, 0);
                // $scope.loading = false;
                content = response.data;
                if (content.status == true) {
                    $http({
                            method: 'POST',
                            url: site_url + 'app/usersession',
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: $.param($scope.formData)+'&type=set&step=2'
                        }).then(function (r) {
                            if (content.data.step) {
                            customerId = content.data.uuid;
                            $scope.page.alert = 'success';
                            $scope.page.flash = content.message;
                            $scope.stepId = content.data.step;
                            $location.path(mainPage + "/step/" + content.data.step);
                            } else {
                            $scope.page.alert = 'success';
                            $scope.page.flash = content.message;
                            $location.path(mainPage + "/step/3");
                            }
                        });
                    
                } else {
                    $scope.page.alert = 'info';
                    $scope.page.flash = content.message;
                }
            }, function error(response) {
                $scope.page.alert = 'info';
                $scope.page.flash = "Something went wrong. Please try again";
                $scope.loading = false;
            });
        } else {
            $window.scrollTo(0, 0);
            $scope.scrollup = false;
            $scope.page.alert = 'info';
            $scope.page.flash = "Please fix errors before proceed";
            $scope.loading = false;
        }
    };
});
app.controller('ctrlStep3', function ($scope, $http, $location, $window) {
    $scope.scrollup = false;
    $scope.checkUUID(3);
    
    $scope.completeSSN = completeSNN;
    $scope.formData = {token: token, uuid: customerId};
    if (typeof ($scope.userData.step3) != 'undefined') {
        $scope.formData = $scope.userData.step3;
    }
    if ($scope.completeSSN) {
        //$('#full-ssn').focus();
    }
    $scope.submitForm = function (isValid) {
        $window.scrollTo(0, 0);
        $scope.scrollup = true;
        // window.parent.scrollTo(0,0);
        // scrltop();
        $scope.loading = true;
        if (isValid == true) {
            $scope.userData.step3 = $scope.formData;
            $http({
                method: 'POST',
                url: apiUrl + 'saveinfo/3',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.formData)
            }).then(function success(response) {
                
                content = response.data;
                // $scope.loading = false;
                if (content.status == true) {
                    $http({
                            method: 'POST',
                            url: site_url + 'app/usersession',
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: $.param($scope.formData)+'&type=set&step=3'
                        }).then(function (r) {
                            $scope.scrollup = false;
                            if(r.paymentDone == 1)
                                $scope.$parent.paymentDone = true;
                            $scope.page.alert = 'success';
                            $scope.page.flash = content.message;
                            $window.scrollTo(0, 0);
                            $location.path(mainPage + "/step/4");
                            
                            /*
                            if (content.data.step) {
                                customerId = content.data.uuid;
                                $scope.page.alert = 'success';
                                $scope.page.flash = content.message;
                                $scope.stepId = content.data.step;
                                $window.scrollTo(0, 0);
                                $location.path(mainPage + "/step/" + content.data.step);
                            } else {
                                $scope.page.alert = 'success';
                                $scope.page.flash = content.message;
                                $window.scrollTo(0, 0);
                                $location.path(mainPage + "/step/3");
                            }
                            */
                        });
                    
                } else {
                    $scope.scrollup = false;
                    $window.scrollTo(0, 0);
                    $scope.page.alert = 'danger';
                    $scope.page.flash = content.message;
                }
            }, function error(response) {
                $window.scrollTo(0, 0);
                $scope.page.alert = 'info';
                $scope.page.flash = "Something went wrong. Please try again";
                $scope.loading = false;
            });
        } else {
            $scope.scrollup = false;
            $window.scrollTo(0, 0);
            $scope.page.alert = 'info';
            $scope.page.flash = "Please fix errors before proceed";
            $scope.loading = false;
        }
    };
    
    //$scope.$parent.loading = false;
    $scope.loading = false;
    // console.log($scope.loading);
});
app.controller('ctrlStep4', function ($scope, $http, $location, $window) {
    $scope.scrollup = false;
    $scope.loading = false;
    $scope.$parent.paymentDone = paymentDone;
    // $scope.paymentDone = paymentDone;
   
    $scope.checkUUID(4);
    // console.log($scope.$parent.paymentDone);
    // $scope.$parent.loading = false;
    $scope.termLoading = false;
    $scope.termFormData = {};
    $scope.pageTitle = 'Terms & Conditions';
    $scope.formData = {token: token, uuid: customerId};
    $scope.termConditions = function () {
        $scope.termLoading = true;
        $scope.loading = true;
        $scope.pageTitle = "Processing your request...please wait.";
        $scope.submitForm();
    };

    $scope.authenticate = function(){
        $scope.scrollup = true;
        $http({
                method: 'POST',
                url: apiUrl + 'mwapi/authenticate',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.formData),
            }).then(function success(response) {
                $scope.scrollup = false;
                
                //Stop loading only if this is a get questions request
                var n = Object.keys($scope.formData).length;
                if(n == 2){
                    $scope.loading = false;
                }
                    
                $window.scrollTo(0, 0);
                content = response.data;
                
                // console.log(content);return false;
                if (content.status == false) {
                    $scope.quizList = false;
                    $scope.formData.question, $scope.formData.answer, $scope.formData.action = '';
                    
                    $scope.loading = false;
                    $scope.termLoading = false;

                    $scope.page.alert = 'danger';
                    $scope.page.flash = content.message;
                    
                    if(content.status_code == 122){
                        completeSNN = true;

                        $location.path(mainPage + "/step/3");
                        return false;
                    }
                    
                    
                    $location.path(mainPage + "/step/4");
                    return false;
                }
                
                switch (content.status_code) {
                    case 100:
                        $scope.questions = content.questions.MultiChoiceQuestion;
                        $scope.quizList = true;
                        $scope.page.alert = 'info';
                        $scope.page.flash = "Please answer the OOW questions";
                        $scope.pageTitle = 'We need to verify your identity, please select the correct answers below.';
                         $scope.loading = false;
                        return false;
                        break;
                    case 101:
                        $scope.page.alert = content.message ? 'danger' : 'info';
                        $scope.page.flash = content.message ? content.message : "We are sorry, we are unable to validate your detail, try to fill them again.";
                        $location.path(mainPage + "/step/3");
                         $scope.loading = false;
                        return false;
                        break;
                    case 122:
                    
                        completeSNN = true;
                        $scope.page.alert = 'info';
                        $scope.page.flash = content.message;
                        $scope.loading = false;
                        $location.path(mainPage + "/step/3");
                        

                        return false;
                        break;
                    case 107:
                        $scope.loading = false;
                        $scope.restartChallenges = true;
                        break;
                    case 109:
                        $scope.authenticated = true;
                        $scope.page.alert = 'success';
                        $scope.page.flash = 'Authentication successfull';
                        $scope.loading = false;
                        $location.path(mainPage + "/step/5");
                        
                        //$scope.page.flash = "Credit Snapshot";
                        return false;
                        break;
                    case 200:
                    // alert(content.status);
                        if(content.status == false){
                /*
                            $scope.page.alert = 'danger';
                            $scope.page.flash = content.message;
                            $location.path(mainPage + "/step/3");
                            return false;*/
                        }
                        $scope.authenticated = true;
                        $scope.page.alert = 'success';
                        $scope.page.flash = 'Authentication successful';
                         $scope.loading = false;
                        $location.path(mainPage + "/step/5");
                        //$scope.page.flash = "Credit Snapshot";
                        return false;
                        break;
                    default:
                        $scope.page.alert = 'danger';
                        $scope.page.flash = content.message;
                         $scope.loading = false;
                        $location.path(mainPage + "/step/1/signup");
                        return false;
                        break;
                }

                
                
            }, function error(response) {
                $scope.scrollup = false;
                $scope.page.alert = 'info';
                $scope.page.flash = "Something went wrong. Please try again";
                $location.path(mainPage + "/step/3");
            });
    }

    $scope.submitForm = function () {
        $scope.scrollup = true;
        $window.scrollTo(0, 0);
        // window.parent.scrollTo(0,0);
        //scrltop();
        $scope.loading = true;
        $scope.page.alert = 'info';
        $scope.page.flash = "Processing your request...please wait.";
        if ($scope.quizList == true) {
            var numquestions = 0;
            var numanswers = 0;
            var quesOpt = {};
            var ansOpt = {};
            $('input[name="QuestionId"]').each(function (index, item) {
                numquestions = ++numquestions;
                var qid = $(this).attr('id');
                var ans = $('.option_' + qid + ':checked').val();
                if (typeof (ans) != 'undefined') {
                    ansOpt[index] = $('.option_' + qid + ':checked').val();
                    quesOpt[index] = $(this).val();
                    numanswers = ++numanswers;
                }
            });
            if (numanswers < numquestions) {
                $scope.page.alert = 'info';
                $scope.page.flash = "All questions are required.";
                return false;
            }
            $scope.page.alert = 'info';
            $scope.page.flash = "Standby as we verify your answers...";
            $scope.loading = true;
            $scope.formData.action = 'verify';
            $scope.formData.question = JSON.stringify(quesOpt);
            $scope.formData.answer = JSON.stringify(ansOpt);
        }
        

        // alert($scope.paymentDone);
        //process payment
        // if($scope.$parent.paymentDone != true){
        if(paymentDone != true){  
            $http({
                method: 'POST',
                url: site_url+'app/payment',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.formData)+ '&' + $.param($scope.userData.step3) + '&' + $.param($scope.userData.step1) + '&' + $.param($scope.userData.step2),
            }).then(function success(response) {
                
                 if(response.data.alert == 'success'){
                    
                    $scope.$parent.paymentDone = true;
                    paymentDone = true;
                    // console.log($scope.$parent.paymentDone);
                    $scope.authenticate();
                 }else{
                    $scope.$parent.paymentDone = false;
                    paymentDone = false;
                    $scope.scrollup = false;
                    $scope.termLoading = false;
                    $scope.page.alert = response.data.alert;
                    $scope.page.flash = response.data.flash;
                    $scope.loading = false;
                    // $location.path(mainPage + "/step/3");
                    return false;
                 }
                
                // console.log(response);return false;
            }, 
            function error(response) {
                $scope.scrollup = false;
                $scope.page.alert = 'info';
                $scope.page.flash = "Could not process your payment. Please try again";
                $location.path(mainPage + "/step/3");
                return false;
            });
        }
        
        if(paymentDone == true){
            $scope.authenticate();

        }
    }
});
app.controller('ctrlStep5', function ($scope, $http, $location, $window, $timeout) {
    $scope.checkUUID(5);
    $scope.loading = true;
    $scope.reportView = false;
    $scope.formData = {token: token, uuid: customerId};
    
    /*
    $http({
        method: 'POST',
        url: apiUrl + 'products',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param($scope.formData),
    }).then(function success(r) {
        $scope.products = r.data.products;
        
        STRIPE_PUBLIC_KEY = r.data.STRIPE_PUBLIC_KEY;
        stripe = Stripe(STRIPE_PUBLIC_KEY);
        elements = stripe.elements();
        var style = {
            base: {
                color: '#32325d',
                lineHeight: '24px',
                fontFamily: 'Helvetica Neue',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        if ($('#card-number-element').length == 0)
            return false;

        //var card = elements.create('card', {style: style});
        cardNumber = elements.create('cardNumber', {style: style});
        cardExpiry = elements.create('cardExpiry', {style: style});
        cardCvc = elements.create('cardCvc', {style: style});
//    card.mount('#card-element');
        cardNumber.mount('#card-number-element');
        cardExpiry.mount('#card-expiry-element');
        cardCvc.mount('#card-cvc-element');
        addEventListener('change', function (event) {
            const displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });
    }, function error(err) {
        $window.scrollTo(0, 0);
    });
*/
    $scope.setProduct = function (id) {
        $scope.page = {};
        $scope.paymentData = {};
        $scope.responseData = {};
        $scope.loading = true;
        $("#productId").val(id);
        $scope.paymentData.uuid = customerId;
        $scope.paymentData.token = token;
        $scope.message = '';

        $scope.paymentData.productAlias = product_alias;
        $http({
            method: 'POST',
            url: apiUrl + 'saveinfo/5',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.paymentData),
        }).then(function success(r) {
            
            $window.scrollTo(0, 0);
            var content = r.data;
            $scope.responseData.alert = (content.status == true) ? 'success' : 'danger';
            $scope.responseData.flash = content.message;
            if (content.status == true) {

                //logout function will clear the session and redirect to MCD dashboard
                //$scope.logout('test');
                //return false;
                
                
                $scope.page.alert = 'success';
                $scope.page.flash = content.message;
                $scope.stepId = content.data.step;
                
                //Dont go to step6.Just show the login button here
                /*
                $timeout(function () {
                    $location.path(mainPage + "/step/" + content.data.step);
                }, 2000);
                */
                $scope.loading = false;
            } else {
                $scope.loading = false;
            }
            $scope.message = content.message;
            $scope.logout_no_redirect();

        }, function error(e) {
            // console.log('there');
            $window.scrollTo(0, 0);
            $timeout(function () {
                $scope.processing = false;
                $scope.page.alert = 'success';
                $scope.page.flash = 'Something went wrong, try later.';
                $location.path(mainPage + "/step/5");
            }, 2000);
        });
    };
    
    $scope.getReport = function () {
        $scope.loading = true;
        $scope.reportView = true;
        $http({
            method: 'POST',
            url: apiUrl + 'mwapi/report',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.formData),
        }).then(function success(r) {
            $window.scrollTo(0, 0);
            $scope.loading = false;
            if (r.data.status == false) {

                $scope.page.alert = 'danger';
                if(r.data.status_code == 118){
                    $scope.page.flash = 'No report data found';
                }else{
                    $scope.page.flash = r.data.status_code == 200 ? r.data.message : "Client is not authenticated yet.";
                    var step = r.data.status_code == 200 ? 6 : 1;
                    $location.path(mainPage + "/step/" + step);
                }
            } else {
                if (r.data.status == true) {
                    $scope.report = r.data.report;
                    $scope.latest = r.data.latest;
                }
            }
            $scope.loading = false;
        }, function error(response) {
            $scope.loading = false;
            $scope.page.alert = 'info';
            $scope.page.flash = "Something went wrong. Please try again";
        });
    };

    $scope.goto = function () {
        $scope.logout('test');
    };
});
app.controller('ctrlStep6', function ($scope, $http, $location, $window) {
    $scope.processing = true;
    $scope.redirectTo = '/';
    $scope.recommendations = [];
    $http({
        method: 'POST',
        url: apiUrl + 'recommendations',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param({token: token, uuid: customerId})
    }).then(function success(response) {
        content = response.data;
        if (content.status == false) {
            $scope.page.alert = 'danger';
            $scope.page.flash = content.message;
            //$location.path(mainPage + 'step/' + content.step);
        } else {
            $scope.recommendations = content.recommendations;
            $scope.redirectTo = content.redirect_to;
        }
        $scope.processing = false;
    }, function error(response) {
        $scope.processing = false;
        $window.scrollTo(0, 0);
        $scope.page.alert = 'info';
        $scope.page.flash = "Something went wrong. Please try again";
    });
    $scope.goto = function () {
        $scope.logout('dashboard');
        $window.location.href = $scope.redirectTo;

    };
});
app.controller('StripeController', function ($scope, $http, $location, $timeout, $window) {
    $scope.paymentData = {};
    $scope.responseData = {};
    $scope.processing = false;
    $scope.paymentData.alert_type = 1;
    $scope.customerName = customerName;
    $scope.submitHandler = function () {
        var form = document.getElementById('payment-form');
        stripe.createToken(cardNumber, cardExpiry, cardCvc).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
                return false;
            } else {
                $scope.paymentData.tokenKey = result.token.id;
                $scope.paymentData.productId = $('#productId').val();
                $scope.paymentData.token = token;
                $scope.paymentData.uuid = customerId;
                $scope.processing = true;

                $http({
                    method: 'POST',
                    url: apiUrl + 'saveinfo/5',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.paymentData),
                }).then(function success(r) {
                    $window.scrollTo(0, 0);
                    var content = r.data;
                    $scope.responseData.alert = (content.status == true) ? 'success' : 'danger';
                    $scope.responseData.flash = content.message;
                    if (content.status == true) {
                        $scope.page.alert = 'success';
                        $scope.page.flash = content.message;
//                        message(content.message, true);
                        $('.pay-close').click();
                        $scope.stepId = content.data.step;
                        $timeout(function () {
                            $location.path(mainPage + "/step/" + content.data.step);
                        }, 2000);
                        $scope.processing = false;
                    } else {
                        $scope.processing = false;
                    }
                }, function error(e) {
                    $window.scrollTo(0, 0);
                    $('.pay-close').click();
                    $timeout(function () {
                        $scope.processing = false;
                        $scope.page.alert = 'success';
                        $scope.page.flash = 'Something went wrong, try later.';
                        $location.path(mainPage + "/step/6");
                    }, 2000);
                });
            }
        });
    };
});

function getQueryString( field ) {
    var url = (window.location != window.parent.location)
        ? document.referrer
        : document.location.href;

    var href = url ? url : '';
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
}; 

function scrltop(){
    $('html, body').animate({
                                     scrollTop: $("body").offset().top
                                       }, 500);
}

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

// To address those who want the "root domain," use this function:
function extractRootDomain() {
    var url = (window.location != window.parent.location)
        ? document.referrer
        : document.location.href;
        console.log(url);
    var domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;

    //extracting the root domain here
    //if there is a subdomain 
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            //this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    return domain;
}
function hidealert(){
    setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
            }, 4000);
}
