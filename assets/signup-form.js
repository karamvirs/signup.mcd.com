document.write('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">');
document.write('<link rel="stylesheet" href="http://signup.mycreditdash.com/assets/signup-form.css">');
function mcd_submit_form(){
	var name = $('#mcd_name').val();
	var email = $('#mcd_email').val();
	var phone = $('#mcd_phone').val();

	
	if(name=='' || email =='' || phone == ''){
		$('#mcd_res').html('All fields are required').show()
		return false;
	}
	if(!email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)){
		$('#mcd_res').html('Enter a valid email').show()
		return false;
	}
	var name_array = name.split(" ");
	var firstname = name_array[0];
	name_array.splice(0,1);
	// console.log(name_array);
	var lastname = name_array.join(" ");
	// console.log(firstname+' | '+lastname);
	
	$('#mcd_res').html('<i class="fa fa-spinner fa-spin"></i>').show();
	$.ajax({
		url: 'http://dev.mycreditdash.com/api/f1/saveinfo',
		type: 'post',
		dataType: 'json',
		data: $('#mcd-signup-form').serialize()+'&firstname='+firstname+'&lastname='+lastname,
		success:function(response){
				// alert(response);
				if (response.status == true) {
					$('#mcd_res').html('Registration Successful.Redirecting to next steps.').show();
				window.location = 'http://107.180.55.213/acr/signup/?t='+$('#token').val()+'&id='+response.data.uuid+'&firstname='+firstname+'&lastname='+lastname+'&email='+email+'&phone='+phone;
				}else{
					$('#mcd_res').html(response.message).show();
				}
		}
	})
}

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

function extractRootDomain(url) {
	var url = (window.location != window.parent.location)
        ? document.referrer
        : document.location.href;
    var domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;

    //extracting the root domain here
    //if there is a subdomain 
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            //this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    return domain;
}